# Ottengo le caselle di tipo speciale

select board.id, contents.text from board, specialboxes, specialtypes, contents where board.specialbox_id = specialboxes.id and specialtypes.id = specialboxes.specialtype_id and specialtypes.content_id = contents.id and contents.lang_id = 1;

# Ottengo le caselle prestiti bancari

select board.id, bankboxes.percentage, banks.name, banks.id as bank_id from board, bankboxes, banks where board.bankbox_id = bankboxes.id and bankboxes.bank_id = banks.id;

# Ottengo le caselle di servizio (pubblicita, consulenze e assicurazioni)

select board.id, shares.name, serviceboxes.price, serviceboxes.dividend, shares.id as share_id from board, serviceboxes,servicetypes,shares where board.servicebox_id = serviceboxes.id and serviceboxes.servicetype_id = servicetypes.id and servicetypes.share_id = shares.id;

#Ottengo le caselle azioni

select board.id, shares.name, shares.price, shares.id as share_id from board, shareboxes,shares where board.sharebox_id = shareboxes.id and shareboxes.share_id = shares.id;

#Ottengo le caselle produzione

select board.id, shares.name, productionboxes.price as box_price, productionboxes.dividend from board, productionboxes,productiontypes,shares where board.productionbox_id = productionboxes.id and productionboxes.productiontype_id = productiontypes.id and productiontypes.share_id = shares.id;

#Ottengo le caselle di distribuzione

select board.id, shares.name, saleboxes.price as sale_price, saleboxes.dividend, saletypes.numsalables from board, saleboxes,saletypes,shares where board.salebox_id = saleboxes.id and saleboxes.saletype_id = saletypes.id and saletypes.share_id = shares.id;

module Engine;
import std.stdio;
import std.exception;
import std.typecons;
import std.string;
import std.conv;
import etc.c.sqlite3;
import std.path;
import std.range;
import std.random;
import std.algorithm;
import std.socket;
import std.format;
import std.math : abs;
import std.concurrency;
import core.thread;
import std.regex;
import std.process;
import srv.Network;
import srv.Msg;
import jsonizer;
import Debug;
import Model;
import Viewer;
import Questions;

private {
	sqlite3 *db;
	Player banker = null;
}
enum {
	zSqlMaxLength = 1024,
	firstColumn = 0,
	startCash = 150_000,
	loanCash = 50_000,
	choosePath = 27,
	endProductionPath = 38,
	beginDistributionPath = 39,
	it = 1,
	address = 1,
	port = 2,
 	numConjunctureCards = 33,
 	numBusinessCards = 33,
 	business = "business",
 	conjuncture = "conjuncture",
 	usrCmdThrowOpaWhite = "WHITE",
 	usrCmdThrowOpaBlack = "BLACK",
 	usrCmdEndOpa = "ENDOPA",
	excDbFinalization = "Errore nella finalizzazione del database, errore %s",
	excDbConnection = "Impossibile connettersi al database, errore %s",
	excDbQuery = "La query { %s } non può essere eseguita, errore: %s",
	excNotEnoughPlayers = "E' necessario avere almeno due giocatori connessi per avviare la partita",
	excNoNameOk = "Errore, non ho ricevuto la risposta nameOk dal server, ricevuto invece: %s",
	excSharesOwnedNull = "Impossibile mostrare le azioni, lista azioni nulla!",
	assDbMissingTable = "Errore: Manca la tabella %s nel database",
	assMissingBoxes = "Mancano caselle per la board, caselle inserite: %s; caselle previste: %s",
	assMissingBoardId = "Manca l'id posizione casella per la board, le query join per inserire caselle nella board devono contenere come PRIMO campo board.id",
	assBoardEmpty = "Impossibile visualizzare una board nulla",
	assOutOfRangePosition = "Il valore della posizione giocatore va oltre i limiti prestabiliti dalla board (1 - %s), posizione giocatore: %s",
	assMissingOpaInformation = "Le informazioni nella board per l'Opa sono nulle!",
	assEmptyPlayerName = "Il nome del giocatore non può essere vuoto o nullo",
	assWrongNumDices = "Errore si possono solo lanciare 1 o 2 dadi. Richiesti %s dadi da lanciare",
	assWrongShareTable = "La classe Share può essere istanziata solo con dati della tabella 'shares'"
}

enum opaDice : int {
	white = 1,
	black,
	spectator
}

enum serviceToken {
	jwt = 1,
	sintex
}


//Configuration paths
string basePath() {
	return expandTilde("~/.manager/");
}

string gladePath() {
	return expandTilde("~/.manager/glade/");
}

class Sqlite {
	//Connessione database
	static void dbConnect(const (char)[] dbFile, int flags) {
		auto status = sqlite3_open_v2(toStringz(dbFile), &db, flags, null);
		enforce((status == SQLITE_OK), format(excDbConnection, status));
	}

	//Wrapper to C sqlite select
	static Tuple!(string[], "columnNames", 
	string[], "data") sqlSelect (const (char)[] from, 
					const (char)[] cols = "*", 
					const (char)[] where = "1", 
					int sqlMaxL = zSqlMaxLength) {
		sqlite3_stmt *result;
		string query = ("SELECT " ~ cols ~ " FROM " ~ from ~ " WHERE " ~ where).idup;
		
		auto status = sqlite3_prepare_v2(db, toStringz(query), sqlMaxL, &result, null);
		enforce((status == SQLITE_OK), format(excDbQuery, query, status));

		mixin(debugInfo("DEBUG: stampa la query"));
		debug writeln(query);
		
		int numCols = sqlite3_column_count(result);
		string[] columnNames;
		foreach (i; 0 .. numCols)
			columnNames ~= to!string(sqlite3_column_name(result, i));
			
		string[] data;
		while (sqlite3_step(result) == SQLITE_ROW) {
			foreach (i; 0 .. numCols)
				data ~= to!string(sqlite3_column_text(result, i));
		}
		status = sqlite3_finalize(result);
		enforce((status == SQLITE_OK), format(excDbFinalization, status));
		
		mixin(debugInfo("DEBUG: mostra i dati raccolti dalla query in formato grezzo"));
		debug writeln(data);
		mixin(debugInfo("DEBUG: mostra i nomi delle colonne ottenuti"));
		debug writeln(columnNames);
		
		return Tuple!(string[], "columnNames", string[], "data")(columnNames, data);
	}
}

class Game {
	Board board;
	//boxRoutines bRoutines;
	int language;
	bool started = false;
	//All shares of the game
	Share[uint] shares;
	Tid gui;
	Socket server;
	
	//Avvia una partita
	this(Table shares, ref Socket server, int language = it) {
		this.language = language;
		this.server = server;
		//Taking all shares frome the parameter (coming from db)
		foreach(row; shares.rows) {
			uint id = to!uint(row["id"]);
			string name = row["name"];
			uint price = to!uint(row["price"]);
			this.shares[id] = new Share(id, name, price);
		}
	}
	
 	void start(string[string] playersInfo, string myName) {
		enforce((playersInfo.length > 1), excNotEnoughPlayers);
		Player[] players;
		//Build all players
		foreach(addressPort, name; playersInfo)
			players ~= new Player(name, addressPort, server);
			
		this.board = new Board(cast(immutable)shares, players);		
		//Initial conditions for debug testing, remove for an official release
		board.players["Tiziana"].sharesOwned = [shares[4].name : shares[4],
							shares[1].name : shares[1],
							shares[2].name : shares[2],
							shares[5].name : shares[5],
							shares[9].name : shares[9]];
		board.players["Tiziana"].loans["Credito Italiano"] = 10;
		board.players["Emily"].jwt = 2;
		board.players["Emily"].sintex = 2;
		board.players["Emily"].products = ["Computers" : 4, "Energy" : 4, "Raw Materials" : 4];
		board.players["Emily"].insurance = 10;
		board.players["Tiziana"].jwt = 2;
		board.players["Tiziana"].sintex = 2;
		//--------------------------------------------------------------------
		gui = locate("guiThread");
		
		this.started = true;
		writeln("GAME STARTED!!");
	}
	
	//Dato un player esegue le azioni della casella in cui si trova
	void boxAction(string myPlayerName) {
		uint position = board.players[myPlayerName].pos();
		string[string] box = board.boxes[position];
		//For debug purpose
		writeln(box);
		//Action!
		board.actionRoutines[position/*20*/](myPlayerName);
		//We ask to the Viewer to refresh my player status output after game action
		gui.sendRequest(UI.refreshPlayerStatus, myPlayerName);
		// *******************************
	}
}

struct Position {
	mixin JsonizeMe;
	@jsonize {
		uint value;
		uint oldValue;
		
		this (uint value, uint oldValue = 1) {
			this.value = value;
			this.oldValue = oldValue;
		}
	}
	
	//Client local flags not sent to network
	bool jwt = false;
	bool sintex = false;
	
	@property uint front() const{
		return value;
	}
	
	@property Position save() const {
		return this;
	}
	
	enum empty = false;
	
	void popFront() {
		switch (value) {
			case choosePath:
				if ((!jwt)&&(!sintex)) {
					bool response;
					//Requesting for Production/Distribution dialog
					response = View.askPath();
					value = (response)? value + 1: beginDistributionPath;
				}
				else if (jwt) {
					value = beginDistributionPath;
					jwt = false;
				}
				else if (sintex) {
					++value;
					sintex = false;
				}
			break;
			case endProductionPath:
				value = 1;
			break;
			default:
				value = (value >= numBoxes)? 1: value + 1;
		}
	}
	
	ref opOpAssign(string op)(in size_t steps) {
		//set oldValue before changing to new value with popFrontN
		oldValue = value;
		if (op == "+")
			this.popFrontN(steps);
		return this;
	}
	
	uint opBinary(string op)(in uint rhs) const {
		uint result;
		if (op == "+")
			result = this.value + rhs;
		else if (op == "-")
			result = this.value - rhs;
		return result;
	}
	
	Position opAssign(Position rhs) {
		oldValue = value;
		this.value = rhs.value;
		return this;
	}
	
	@property uint opCall() const{
		return value;
	}
	
	@property uint getOld() const {
		return oldValue;
	}
	
	int opCmp(int o) const {
		return cast(int)this.value - o;
	}
	
	uint opCast(T : uint)() const {
		return value;
	}
	
	bool useServiceToken(int tokenType, ref uint nJwt, ref uint nSintex) {
		oldValue = value;
		switch (tokenType) {
				case serviceToken.jwt:
					//JWT token
					if (nJwt > 0) {
						this.jwt = true;
						this.sintex = false;
						--nJwt;
						this.value = choosePath;
					}
					else 
						return false;
				break;
				case serviceToken.sintex:
					//sintex token
					if (nSintex > 0) {
						this.sintex = true;
						this.jwt = false;
						--nSintex;
						this.value = choosePath;
					}
					else
						return false;
				break;
				default:
					return false;
				break;
		}
		return true;
	}
}

class Player {
	mixin JsonizeMe;
	@jsonize {
		string name;
		int cash;
		uint[string] loans;
		Position pos;
		uint diceNum;
		uint jwt;
		uint sintex;
		uint insurance;
		string addressPort;
		uint[string] products;
		//Id delle azioni possedute
		Share[string] sharesOwned;
		bool lost;
	}
	Socket server;
	Tid gui;
	Board board;
	
	//Restituisce il numero di prodotti finiti posseduti
	@property uint ownedProducts() const
	{
		return reduce!((a, b) => min(a, b))(uint.max, products.byValue());
	}
	
	invariant {
		assert(((pos > 0)&&(pos <= numBoxes)), format(assOutOfRangePosition, numBoxes, pos));
		assert((name !is null)&&(name != ""), assEmptyPlayerName);
		assert(((diceNum == 1)||(diceNum == 2)), format(assWrongNumDices, diceNum));
	}
	
	//_ctor used by normal instantiation and not by jsonize deserialization
	this(string name, string addressPort, Socket server, 
	     int cash = startCash, Position pos = Position(1), 
	     uint[string] loans = ["Banco di Roma" : 0, "Credito Italiano" : 0],
	     uint[string] products = ["Computers" : 0, "Energy" : 0, "Raw Materials" : 0],
	     Share[string] sharesOwned = null, bool lost = false, uint diceNum = 2, 
	     uint jwt = 0, uint sintex = 0, uint insurance = 0) {
		
		gui = locate("guiThread");
		this.name = name;
		this.addressPort = addressPort;
		this.cash = cash;
		this.pos = pos;
		this.products = products.dup;
		this.loans = loans.dup;
		this.sharesOwned = sharesOwned;
		this.lost = lost;
		this.diceNum = diceNum;
		this.jwt = jwt;
		this.sintex = sintex;
		this.insurance = insurance;
		this.server = server;
	}
	//jsonize _ctor to rebuild Player object on deserialization. This _ctor does not set the socket
	@jsonize this(string name, string addressPort, 
		      int cash = startCash, Position pos = Position(1), 
		      uint[string] loans = ["Banco di Roma" : 0, "Credito Italiano" : 0],
		      uint[string] products = ["Computers" : 0, "Energy" : 0, "Raw Materials" : 0],
		      Share[string] sharesOwned = null, bool lost = false, uint diceNum = 2, 
		      uint jwt = 0, uint sintex = 0, uint insurance = 0) {
		this.name = name;
		this.addressPort = addressPort;
		this.cash = cash;
		this.pos = pos;
		this.products = products.dup;
		this.loans = loans.dup;
		this.sharesOwned = sharesOwned;
		this.lost = lost;
		this.diceNum = diceNum;
		this.jwt = jwt;
		this.sintex = sintex;
		this.insurance = insurance;
	}
	
	void setBoard(Board board) {
		this.board = board;
	}
	
	void updateWith(Player updatedPlayer) {  
		    this.cash = updatedPlayer.cash;
		    this.loans = updatedPlayer.loans;
		    this.pos = updatedPlayer.pos;
		    this.diceNum = updatedPlayer.diceNum;
		    this.jwt = updatedPlayer.jwt;
		    this.sintex = updatedPlayer.sintex;
		    this.insurance = updatedPlayer.insurance;
		    this.products = updatedPlayer.products;
		    this.sharesOwned = updatedPlayer.sharesOwned;
	}
	
	int[] throwDices(ref Game game, bool updatePosition = true) {
		version(gtk) {
			auto dices = generate!(() => uniform(1, 7));
			int[] diceValues = dices.take(diceNum).array;
			if (updatePosition)
				pos += diceValues.sum;
			return diceValues;
		}
	}
	
	void newShare(string[string] shareBox) {
		writeln("SHARE DEBUG: ", shareBox);
		string owner = whoIsTheOwnerOf(shareBox["name"]);
		
		//If the player is the owner then asks for share selling
		if (owner == this.name) {
			gui.sendRequest(UI.showPopupAskSellShareFull, format(questSellShareFull, shareBox["name"]));
			//Checking response
			if (receiveOnly!bool)
				sell(sharesOwned[shareBox["name"]], 120);
		}
		//If no owner then asks for share purchasing
		else if (owner is null) {
			gui.sendRequest(UI.showPopupAskPurchaseShare, format(questSellPurchaseShare, shareBox["name"], shareBox["price"]));
			//Checking response
			if (receiveOnly!bool)
				purchase(board.shares[to!uint(shareBox["share_id"])], 100);

		}
		//If the player is NOT the owner then asks for throwing OPA
		else {
			gui.sendRequest(UI.showPopupAskOpa, format(questThrowOpa, shareBox["name"]));
			//Checking response
			//We ask for OPA dialog. We will throw the white dice in this context
			//if true then throw NEW OPA!!
			if (receiveOnly!bool) {
				//We setup the opaInfo to board
				board.opaInfo["throwerName"] = this.name;
				board.opaInfo["ownerName"] = owner;
				board.opaInfo["shareId"] = to!string(shareBox["share_id"]);
				board.opaRunning = true;
				
				//View.opaDialog(opaDice.black);
				gui.sendRequest(UI.showOpaDialog, to!string(to!int(opaDice.black)));
				//We notify to the server that we are thrwoing a new opa with all information
				server.sendAll(plyCmdThrowOpa ~ serialize(board.opaInfo));
			}
		}
			
	}
	
	string throwOpaDice(int type) {
		string[] opaValues;
		
		if (type == opaDice.black)
			opaValues = ["-20%", "-20%", "-40%", "-60%", "-60%", "EXPIRED"];
		else if (type == opaDice.white)
			opaValues = ["20%", "40%", "60%", "EXPIRED", "EXPIRED", "EXPIRED"];

		auto opaDiceValue = generate!(() => opaValues[uniform(0, 6)]);
		return opaDiceValue.take(1).array[0];
	}
	
	void setDiceNum(uint diceNum) {
		this.diceNum = diceNum;
	}
	
	void lose() {
		this.cash = 0;
		this.loans = null;
		this.products = null;
		this.sharesOwned = null;
		this.lost = true;
		server.sendAll(plyCmdNotify ~ format(msgBankruptcy, this.name));
	}
		
	string whoIsTheOwnerOf(string shareName) {
		foreach (player; board.players) {
			if (shareName in player.sharesOwned)
				return player.name;
		}
		return null;
	}
	
	//Takes a loan on a box or it asks for debt return if the loan is already taken for this bank
	void takeLoan(in string[string] box) {
			uint boxPercentage = to!uint(box["percentage"]);
			string bankName = box["name"];
			uint loanPercentage = this.loans[bankName];
			bool iOwnTheBank = ((bankName in this.sharesOwned) !is null);
			uint cashToReturn = loanCash;
			uint interest = loanCash * loanPercentage / 100;
			
			//Restituzione prestito (piu vincoli di congiuntura)
			if ((loanPercentage != 0) && 
			    (board.conjuncture.upCard.conjunctureFlag != conjunctureFlags.noReturnCashLoan)) {
				pay(cashToReturn, banker);
				string creditorName = whoIsTheOwnerOf(bankName);
				if (!iOwnTheBank) {
					Player creditor = (creditorName is null)? null: board.players[creditorName];
					pay(interest, creditor);
				}
				this.loans[bankName] = 0;
				if (!this.lost) {
					//If no bankruptcy Let's notify the operation
					if (creditorName !is null)
						server.sendAll(plyCmdNotify ~ format(msgLoanReturnedDividend, this.name, bankName, creditorName, interest));
					else
						server.sendAll(plyCmdNotify ~ format(msgLoanReturned, this.name, bankName, loanPercentage));
				}
			}
			//Chiedere se contrarre il prestito (piu vincoli di congiuntura)
			else if ((loanPercentage == 0) && 
				((boxPercentage != 10) || (board.conjuncture.upCard.conjunctureFlag != conjunctureFlags.noTenPercentLoan)) &&
				 (View.askLoan(boxPercentage, bankName))) {
					this.cash += loanCash;
					this.loans[bankName] = boxPercentage;
					//View.showMessage(msgLoanTaken);
				//Message notify operation
				server.sendAll(plyCmdNotify ~ format(msgLoanTaken, this.name, bankName, boxPercentage));
			}
	}
	
	//Paga al giocatore creditor se non nullo altrimenti paga al cassiere
	void pay(uint amount, ref Player creditor) {
		this.cash -= amount;
		if (creditor !is null) {
			creditor.cash += amount;
			server.sendAll(plyCmdPlayerUpdate ~ serialize(creditor));
			server.sendAll(plyCmdPlayerUpdate ~ serialize(this));
		}
		if (this.cash < 0) {
			bool bankruptcy = sellSharesForDebt();
			if (bankruptcy) {
				//View.showMessage(msgLost);
				server.sendAll(plyCmdLost);
				lose();
			}
		}
	}
	
	//Buys a share as percentage of price
	bool purchase(immutable Share share, uint percentage, bool opa = false) {
		string owner = whoIsTheOwnerOf(share.name);
		if ((owner !is null)&&(opa == false))
			return false;
		else {
			//If there is an owner we need to delete the ownership
			if (owner !is null) {
				board.players[owner].sharesOwned.remove(share.name);
				board.players[owner].cash += share.price * (percentage / 100);
			}
				
			//Then we add the ownership to this player
			sharesOwned[share.name] = cast(Share)share;
			//Paying it of course xD
			this.cash -= share.price * (percentage / 100);
			
			return true;
		}
	}
	
	//Sells product/products (at least 1 raw material, 1 energy, 1 computer)
	void sellProducts(in string[string] box) {
		uint price = to!uint(box["sale_price"]);
		uint dividend = to!uint(box["dividend"]);
		uint numSalables = to!uint(box["numsalables"]);
		string shareName = box["name"];
		string shareHolderName = whoIsTheOwnerOf(shareName);
		if ((ownedProducts >= numSalables)&&(View.askSellProducts(price, dividend, numSalables, shareHolderName))) {
			this.cash += price;
			foreach(ref value; this.products.byValue)
				value -= numSalables;
				
			string notifyMessage;
			if (shareHolderName !is null) {
				board.players[shareHolderName].cash += dividend;
				notifyMessage = format(msgSoldProducts, this.name, numSalables, price, shareHolderName, dividend);
			}
			else
				notifyMessage = format(msgSoldProductsNoShareHolder, this.name, numSalables, price);
			server.sendAll(plyCmdNotify ~ notifyMessage);
		}
	}
	
	
	//Sells a share as percentage of price
	bool sell(Share share, uint percentage) {
		//we check if we own this share to sell it otherwise we return false
		string owner = whoIsTheOwnerOf(share.name);
		if (owner != this.name)
			return false;

		uint gain = share.price * (percentage / 100);
		this.cash += gain;
		sharesOwned.remove(share.name);
		
		return true;
	}
	
	//Vendere alla banca per debiti
	bool sellSharesForDebt() {
		bool bankruptcy = true;
		while ((this.cash < 0) && (sharesOwned.length > 0)) {
			foreach(shareName, shareInfo; sharesOwned) {
				//TODO: use sell function
				uint shareEarning = shareInfo.price * 3 / 5; //60% del valore
 				uint debt = abs(this.cash);
				this.cash += (View.askSellShare(shareInfo.name, shareEarning, debt))? shareEarning: 0;
				sharesOwned.remove(shareName);
				if (this.cash >= 0) {
					bankruptcy = false;
					break;
				}
			}
		}
		//View.showMessage(format(msgRemainingCash, this.cash));
		
		return bankruptcy;
	}
	
	//Products or services tokens buy
	void buyTokens(in string[string] box) {
		string tokenType = box["name"];
		uint price = to!uint(box["price"]);
		uint dividend = to!uint(box["dividend"]);
		if (!isCashEnough(price))
			gui.sendRequest(UI.showPopupMessage, format(msgNotEnoughCash, tokenType));
		else if (uint nTokens = View.askBuyTokens(tokenType, price, dividend)) {
			price *= nTokens;
			dividend *= nTokens;
			pay(price - dividend, banker);
			string shareHolderName = whoIsTheOwnerOf(tokenType);
			Player playerToPay = (shareHolderName is null)? banker: board.players[shareHolderName];
			pay(dividend, playerToPay);
			
			final switch (tokenType) {
				case "jwt":
					this.jwt += nTokens;
				break;
				case "sintex":
					this.sintex += nTokens;
				break;
				case "Compagnia di Assicurazione Milano":
					this.insurance += nTokens;
				break;
				case "Dalmine":
					this.products["Raw Materials"] += nTokens;
				break;
				case "Enel":
					this.products["Energy"] += nTokens;
				break;
				case "Olivetti":
					this.products["Computers"] += nTokens;
				break;
			}
			
			//Let's notify a message to all about this operation
			string notifyMessage;
			if (playerToPay !is null)
				notifyMessage = format(msgBoughtTokenDividend, this.name, nTokens, tokenType, shareHolderName, dividend);
			else
				notifyMessage = format(msgBoughtToken, this.name, nTokens, tokenType);
			server.sendAll(plyCmdNotify ~ notifyMessage);
		}
	}
	
	void takeConjuncture() {
		board.conjuncture.takeOneCard();
		gui.sendRequest(UI.printConjuncture, board.conjuncture.upCard.text);
		//notify to server we have taken one card so other players will synchronize this action
		server.sendAll(plyCmdConjuncture);
	}
	
	void takeBusinessCard() {
		auto card = board.business.takeOneCard();
		server.sendAll(plyCmdBusiness);
		if (card.isEmpty)
			card = board.business.takeOneCard();
		//Only for the player that picked up a new card, we show the text as popup
		gui.sendRequest(UI.showPopupMessage, card.text);
		mixin CardProcedures!business;
		//it calls the card's procedure
		businessProcedures[/*card.id*/businessId.fiveRawMaterialsPay]();
	}
	
	void accident(in string[string] box) {
		string notifyMessage;
		string popupMessage;
		uint tokenUsed;
		uint boxId = to!uint(box["id"]);
		//We get the type of damage from box information
	        if (boxId == accidentBoxType.rawMaterials) {
			if (products["Raw Materials"] == 0) {
				notifyMessage = format(msgNoRawMaterials, this.name);
				popupMessage = msgNoRawMaterialsPopup;
			}
			else if (products["Raw Materials"] == 1) {
				tokenUsed = useInsurance(15000);
				products["Raw Materials"]--;
				notifyMessage = format(msgFireRawMaterials, this.name, 1) ~ "\n" ~
						((tokenUsed > 0) ? format(msgInsuranceCompensation, this.name, 15000) :
								  format(msgNoInsurance, this.name));
				popupMessage = format(msgFireRawMaterialsPopup, 1) ~ "\n" ~
						((tokenUsed > 0) ? format(msgInsuranceCompensationPopup, 15000) :
								  msgNoInsurancePopup);
			}
			else {
				tokenUsed = useInsurance(30000);
				products["Raw Materials"] -= 2;
				notifyMessage = format(msgFireRawMaterials, this.name, 2) ~ "\n" ~
						((tokenUsed > 0) ? format(msgInsuranceCompensation, this.name, 30000) :
								  format(msgNoInsurance, this.name));
				popupMessage = format(msgFireRawMaterialsPopup, 2) ~ "\n" ~
					       ((tokenUsed > 0) ? format(msgInsuranceCompensationPopup, 30000) :
								 msgNoInsurancePopup);
			}
	        }
		else if (boxId == accidentBoxType.products) {
			if (ownedProducts == 0) {
				notifyMessage = format(msgNoProductsOwned, this.name);
				popupMessage = msgNoProductsOwnedPopup;
			}
			else if (ownedProducts > 0) {
				tokenUsed = useInsurance(36000);
				//We remove 1 product (all component of 1 product: 1 rawMat, 1 energy, 1 computer)
				foreach (ref component; products)
					component--;
				notifyMessage = format(msgFireProducts, this.name) ~ "\n" ~
						((tokenUsed > 0) ? format(msgInsuranceCompensation, this.name, 36000) :
								  format(msgNoInsurance, this.name));
				popupMessage = msgFireProductsPopup ~ "\n" ~
						((tokenUsed > 0) ? format(msgInsuranceCompensationPopup, 36000) :
								  msgNoInsurancePopup);
			}
		}
		
		server.sendAll(plyCmdNotify ~ notifyMessage);
		gui.sendRequest(UI.showPopupMessage, popupMessage);
	}

	uint useInsurance(uint compensation, uint amount = 1) {
		uint tokenUsed = 0;
		if (this.insurance >= amount) {
			this.cash += compensation * amount;
			this.insurance -= amount;
			tokenUsed = amount;
		}
		else if (this.insurance == 1) {
			this.cash += compensation;
			this.insurance--;
			tokenUsed = 1;
		}
		
		return tokenUsed;
	}
	
	bool isCashEnough(uint payment) {
		return (this.cash >= payment);
	}
}//End class Player

auto startServer(string port) {
	auto pid = spawnProcess(["srv/managerServer", port]);
	return pid;
}

void sendRequest(Tid gui, int request, string data) {
		if (gui != Tid.init)
			gui.send(request, data, thisTid);
		else {
			writefln("Attempted request %s with data %s to null thread! Trying with guiThread", request, data);
			Tid defaultThread = locate("guiThread");
			defaultThread.send(request, data, thisTid);
		}
}

void sendStartHigherNumberDices(Socket server, bool iAmDirector) {
	if (iAmDirector) {
		auto sent = server.sendAll(plyCmdHigherDices);
		writeln("Comando " ~ plyCmdHigherDices ~ " inviato");
	}
}

void sendPlayerName(Socket server, string name) {
	server.sendAll(plyCmdName ~ name);
	writeln("Comando " ~ plyCmdName ~ " inviato");
}


// ---------------- THREADS --------------------
//Thread chatSenderThread
void chatSender(shared Socket sock) {
	Socket server = cast(Socket) sock;
	writeln("chatSender thread started");
	while(true)
	{
		string msg = receiveOnly!string;
		server.sendAll(plyCmdMsg ~ msg);
	}
}

//Thread eListenerThread
void eListener() {
	writeln("eListener thread started");
	Tid mainThread;
	string cmd;
	while (true) {
		//Waiting from netTalker thread turn/play command
		do {
			cmd = receiveOnly!string;
		}
		while((cmd != srvRspTurn)&&(cmd != srvCmdPlay));
		//Waiting for user input signal, if received a dice event then thread will need to stop listening user input till next turn/play
		string signal;
		do {
			signal = receiveOnly!string;
			ownerTid.send(cmd, signal);
		}
		while(signal != "dice");
	}
}

//Thread netTalker
void netTalker(shared Socket sock, shared bool director) {
	Socket server = cast(Socket) sock;
	bool iAmDirector = cast(bool) director;
	writeln("Thread netTalker started");
	while(true)
	{
		char[][] packets;
		auto received = server.receiveAll(packets);

		if (received > 0) {
			//looping all packets per receiveAll
			foreach (packet; packets) {
				mixin Interpreter!nTalker;
				evaluate(matchCommand(packet, syntax));
			}
		}
	}
}

//Thread guiThread
void guiThreadAdapter(shared Socket sharedServer, shared bool sharedIAmDirector) {
	version (gtk) {
		Socket server = cast(Socket)sharedServer;
		bool iAmDirector = cast(bool)sharedIAmDirector;
		View.guiThread(server, iAmDirector);
	}
}

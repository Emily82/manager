module Questions;

enum {
	questPath = "Vuoi andare nel percorso Produzione o Distribuzione?",
	questLoan = "Vuoi prendere un prestito di 50000$ al %s%% presso la banca %s?",
	questSellShare = "Liquidità insufficiente per pagare i tuoi debiti.\n\nVendendo l'azione %s ricaverai %s $ (60%% del valore), va bene?\nDebito da restituire:%s $ ",
	questSellShareFull = "Desideri vendere l'azione %s al 120%% del suo valore?",
	questSellPurchaseShare = "Desideri acquistare l'azione %s al prezzo di $%s?",
	questSellProducts = "Desideri vendere %s prodotti finiti al prezzo di $%s? Il giocatore %s riceverà  un dividendo di $%s",
	questSellProductsNoShareHolder = "Desideri vendere %s prodotti finiti al prezzo di $%s?",
	questAskBuyTokens = "Vuoi acquistare gettoni %s al prezzo di %s$ di cui %s$ all'azionista?",
	questHowManyTokens = "Quanti gettoni vuoi acquistare? ",
	questDices = "Quanti dadi vuoi lanciare? ",
	questName = "Nome giocatore: ",
	questAddress = "Indirizzo connessione server: ",
	questPort = "Indirizzo porta server: ",
	questThrowOpa = "Vuoi lanciare un'O.P.A. sull'azione %s?"
}
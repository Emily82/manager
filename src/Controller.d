module Controller;
import std.stdio;
import std.conv;
import std.string;
import std.format;
import std.socket;
import srv.Network;
import srv.Msg;
import std.exception;
import std.concurrency;
import std.algorithm;
import std.regex;
import core.thread;
import Model;
import Debug;
import Engine;
import Viewer;

alias SocketType = std.socket.SocketType;

unittest {
	auto database = new Database(basePath ~ "data.db");
	database.sanityCheck();
}

int main(string[] argv) {
	string serverAddress;
	ushort serverPort;
	//try {
	enforce((argv.length >= 3), excNotEnoughArguments);
	serverAddress = argv[address];
	serverPort = to!ushort(argv[port]);
	/*}
	catch (Exception e) {
		auto serverInfo = View.askServerAddress(argv);
		serverAddress = serverInfo.address;
		serverPort = serverInfo.port;
	}*/

	//Avvia il server in ascolto sulla stessa porta del client
	/*auto pid = startServer(argv[port]);
	scope(exit) wait(pid);*/
	
	auto database = new Database(basePath ~ "data.db");
	
	auto server = new Socket(AddressFamily.INET, SocketType.STREAM, ProtocolType.TCP);
	server.setOption(SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, true);
	server.connect(new InternetAddress(serverAddress, serverPort));
	
	auto game = new Game(database.getTable("shares"), server);
	
	bool iAmDirector = ((serverAddress == "localhost")||(serverAddress == "127.0.0.1"));
	//Let's register the main thread
	register("mainThread", thisTid);
	//Starting guiThread
	Tid gui = spawn(&guiThreadAdapter, cast(shared)server, cast(shared)iAmDirector);
	register("guiThread", gui);

	string name = receiveOnly!string;
	writeln(name);
	//thisTid.setMaxMailboxSize(10, OnCrowding.block);
	//debugNetworkShell(server);
	//Avvio il thread netTalker
	auto nTalker = spawn(&netTalker, cast(shared) server, cast(shared) iAmDirector);
	register("netTalker", nTalker);
	//Lets start the chat thread
	auto chatSenderThread = spawn(&chatSender, cast(shared) server);
	register("chatSenderThread", chatSenderThread);
	//Lets start the eventListener
	auto eListenerThread = spawn(&eListener);
	register("eListenerThread", eListenerThread);
	//Invio il nome del mio player e al server e attendo l'ok
	server.sendPlayerName(name);
	//Here we must catch manually the server nameOK receivedCmd to block the receive just below in the code till the game start
	string receivedCmd = receiveOnly!string;
	//Checking if we have received a nameOk or it will throw an exception
	enforce((receivedCmd.matchFirst(syntax[srvRspNameOk])), format(excNoNameOk, receivedCmd));
	
	//View.askStart(server, iAmDirector);
	//Instantiated when receiving begin message from netTalker (see Network.d)
	Player myPlayer;

	while(true) {
		//Waiting for netTalker thread sends to be evaluated
		receive(
			(string receivedCmd) {
				string opaValue;
				//First we handle internal thread commands (ex: throw OPA dice)
				switch (receivedCmd) {
					case usrCmdThrowOpaBlack :
						opaValue = myPlayer.throwOpaDice(opaDice.black);
						//Render our opa dice
						//gui.sendRenderOpaDiceRequest(opaValue, opaDice.black);
						string[] params = [opaValue, to!string(to!int(opaDice.black))];
						writeln("PARAMS: ", params);
						gui.sendRequest(opaUI.renderOpaDice, serialize(params));
						//We communicate our opa dice value to the server
						server.sendAll(plyCmdOpaBlack ~ opaValue);
					break;
					case usrCmdThrowOpaWhite :
						opaValue = myPlayer.throwOpaDice(opaDice.white);
						//Render our opa dice
						string[] params = [opaValue, to!string(to!int(opaDice.white))];
						writeln("PARAMS: ", params);
						gui.sendRequest(opaUI.renderOpaDice, serialize(params));
						//gui.sendRenderOpaDiceRequest(opaValue, opaDice.white);
						//We communicate our opa dice value to the server
						server.sendAll(plyCmdOpaWhite ~ opaValue);
					break;
					case usrCmdEndOpa :
						game.board.opaRunning = false;
						//if we are the thrower then we send the terminate opa command to the server
						//We can finally send the endturn if we are the thrower
						if (myPlayer.name == game.board.opaInfo["throwerName"]) {
							server.sendAll(plyCmdOpaTerminated);
							server.sendAll(plyCmdEndTurn ~ serialize(myPlayer));
						}
					break;
					default :
						/*
						Network commands forwarded by netTalker thread. The interpreter is defined in Network.d
						We need to define an empty signal var to avoid problems with the mixin template
						*/
						string signal;
						mixin Interpreter!controller;
						evaluate(matchCommand(receivedCmd, syntax));
				}	
			},
			(string receivedCmd, string signal) {
				//again network commands
				mixin Interpreter!controller;
				evaluate(matchCommand(receivedCmd, syntax));
			},
			(Variant receivedCmd) {
				writefln("mainThread: unhandled network thread message <%s>", receivedCmd);
			}
		);
	}//while(true)
}//main()

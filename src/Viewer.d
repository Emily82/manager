module Viewer;
import std.stdio;
import std.format;
import std.string;
import std.concurrency;
import std.conv;
import std.typecons;
import std.socket;
import std.regex;
import core.thread;
import gtk.Main;
import gtk.Box;
import gtk.Frame;
import gtk.Window;
import gtk.MainWindow;
import gtk.Layout;
import gtk.Button;
import gtk.Entry;
import gtk.Label;
import gtk.EventBox;
import gtk.Widget;
import gtk.CssProvider;
import gtk.StyleContext;
import gtk.TreeView;
import gtk.TreePath;
import gtk.TextView;
import gtk.TreeSelection;
import gtk.TreeViewColumn;
import gtk.ListStore;
import gtk.CellRendererText;
import gtk.CellRendererPixbuf;
import gtk.TreeIter;
import gtk.Grid;
import gtk.Image;
import gtk.SpinButton;
import gtk.Builder;
import gtk.PopupBox;
import gtk.MessageDialog;
import gtk.Application: Application;
import gtk.ApplicationWindow: ApplicationWindow;
import gdk.Event;
import gdk.Color;
import gdk.Screen;
import gdk.Display;
import glib.Idle;
import glib.MainLoop;
import glib.MainContext;
import gio.Application: GioApplication = Application;
import gtkc.giotypes: GApplicationFlags;
import gobject.Type;
import srv.Msg;
import Model;
import Engine;
import Questions;
import Debug;
import Network;

alias StateType = gtkc.gtktypes.StateType;
/*alias quitMainEventLoop = Main.quit;
alias startMainEventLoop = Main.run;
alias initMainEventLoop = Main.init;*/
/*alias threadsAddIdle = gdk.Threads.threadsAddIdle;
alias threadsAddIdleFull = gdk.Threads.threadsAddIdleFull;
alias threadsAddIdle = Idle.add;
alias threadsAddIdleFull = Idle.addFull;*/

enum GTK_STYLE_PROVIDER_PRIORITY_APPLICATION = 600;
enum GTK_STYLE_PROVIDER_PRIORITY_USER = 800;

enum guiSchedulerTimeout = 10;

template ifGtkEnabled(T) {
    version(gtk) {
        alias ifGtkEnabled = T;
    } else {
        interface ifGtkEnabled {}
    }
}

enum UI : int {
	enableUserButtons = 1,
	disableUserButtons,
	enableServiceButtons,
	disableServiceButtons,
	enableDiceButton,
	disableDiceButton,
	refreshPlayerStatus,
	fullRefreshPlayer,
	renderDices,
	renderMessage,
	printMessage,
	printConjuncture,
	showPopupMessage,
	showPopupAskPath,
	showPopupAskLoan,
	showPopupAskSellShare,
	showPopupAskSellShareFull,
	showPopupAskSellProducts,
	showPopupAskPurchaseShare,
	showPopupAskOpa,
	showAskBuyTokens,
	showAskHowManyTokens,
	showOpaDialog,
	alert
}

enum opaUI : int {
	disableOpaDice = 100,
	enableOpaDice,
	disableCloseButton,
	enableCloseButton,
	refreshThrowerCash,
	renderOpaDice,
	showOpaResult
}

//Graphics before game start

//*************** Secondary Apps *****************
abstract class FormWindow : ApplicationWindow {
	Application application;
	Window w;
	Button ok;
	Entry[] input;
	string gladeFile;
	
	this (Application application, string gladeFile) {
		super(application);
		this.application = application;
		this.gladeFile = gladeFile;
		//This will call the implemented setupUI
		setupUI();
		foreach(textbox; input)
			textbox.addOnActivate(&submit!Entry);
		w.showAll();
	}
	
	T getUserData(T)() {
		T result;
		foreach (textbox; input)
			result ~= textbox.getText();
		w.destroy();
		return result;
	}
	
	abstract void setupUI();
	
	void submit(T)(T obj) {
		closeWindow();
	}
	
	void closeWindow() {
		w.hide();
		application.quit();
	}
}

class AskNameForm : FormWindow {
	this (Application application, string gladeFile) {
		super(application, gladeFile);
	}
	
	//Called by super _ctor
	override void setupUI() {
		Builder b = new Builder();
		b.addFromFile(gladeFile);
		w = cast(Window)b.getObject("window1");
		input ~= cast(Entry)b.getObject("txtName");
		ok = cast(Button)b.getObject("btnOk");
		ok.addOnClicked(&submit!Button);
	}
}

class AskAddressForm : FormWindow {
	this (Application application, string gladeFile) {
		super(application, gladeFile);
	}
	
	//Called by super _ctor
	override void setupUI() {
		Builder b = new Builder();
		b.addFromFile(gladeFile);
		w = cast(Window)b.getObject("window1");
		input ~= cast(Entry)b.getObject("txtAddress");
		input ~= cast(Entry)b.getObject("txtPort");
		ok = cast(Button)b.getObject("btnOk");
		ok.addOnClicked(&submit!Button);
	}
}

class WaitingRoomWindow : FormWindow {
	TreeView listView;
	TreeViewColumn name;
	TreeViewColumn address;
	Button refresh;
	Button start;
	Socket server;
	bool iAmDirector;
	Idle engine;
	
	this (Application application, string gladeFile, ref Socket server, bool iAmDirector) {
		//First init the socket to permit to setupUI function (called by superconstructor) to get players list from server
		this.server = server;
		this.iAmDirector = iAmDirector;
		super(application, gladeFile);
		refresh.addOnClicked(&refreshPlayersList!Button);
		start.setSensitive(iAmDirector);
		if (iAmDirector)
			start.addOnClicked(&tryToStart!Button);
		engine = new Idle(&starter, GPriority.DEFAULT_IDLE, true);
	}
	
	/*
	This idle will close the window and app receiving from netTalker the begin packet
	then it will send the packet to mainThread for game start
	*/
	bool starter() {
		receiveTimeout(dur!("msecs")( guiSchedulerTimeout ), 
				(string signal) {
					if ((signal.matchFirst(srvRspBegin)) && (!iAmDirector)) {	
						application.quit();
						w.destroy();
						engine.stop();
						Tid mainThread = locate("mainThread");
						mainThread.send(signal);
					}
				}
		);
		return true;
	}
	
	//Called by super _ctor
	override void setupUI() {
		Builder b = new Builder();
		b.addFromFile(gladeFile);
		w = cast(Window)b.getObject("window1");
		
		listView = cast(TreeView)b.getObject("playersList");
		listView.setFixedHeightMode(false);
		listView.setModel(getPlayersList());
		
		name = new TreeViewColumn("Nome", new CellRendererText(), "text", 0);
		listView.appendColumn(name);
		address = new TreeViewColumn("Indirizzo", new CellRendererText(), "text", 1);
		listView.appendColumn(address);
		//We put inherited default ok button to be the start button
		start = cast(Button)b.getObject("btnStartServer");
		ok = start;
		refresh = cast(Button)b.getObject("btnRefresh");
	}
	
	//Here the director has clicked "start server"
	void tryToStart(T)(T obj) {
		if (iAmDirector) {
			//We have to make a last request to check if the server has given us more than 2 players
			//updater.stop();
			auto list = getPlayersList();
			//Updates the listView converting to PlayersList type the string[]
			listView.setModel(list);
			//getPlayersList();
			//server.sendAll(plyCmdStart);
			if (list.length > 1) {
				application.quit();
				w.destroy();
			}
			else
				PopupBox.information(msgNotEnoughPlayers, "Information");
		}
	}
	
	PlayersList getPlayersList() {
		PlayersList list = new PlayersList();
		//Sending new list request to the server
		server.sendAll(plyCmdGetPlayersList);
		//Waiting, from netTalker thread, the new list received by the server
		string data = receiveOnly!string;
		writeln("getPlayersList: received ", data);
		auto clientInfo = deserialize!(string[string])(data);
		foreach(playerAddress, playerName; clientInfo)
			list.addPlayer(playerName, playerAddress);
		return list;
	}
	
	//Event handler:get players list from server and set it to the list glade object
	void refreshPlayersList(T)(T obj) {
		listView.setModel(getPlayersList());
	}
}

//Graphics after game start

class OpaWindow : MainWindow {
	Window w;
	string gladeFile;
	int type;
	immutable Board board;
	Button btnOpaDice, btnClose;
	Label cashWidget, opaDescription, labelThrower, labelOwner;
	string throwerName, ownerName, opaShareName;
	Frame throwerFrame, ownerFrame;
	Image blackDice, whiteDice;
	
	this (immutable Board board, int type) {
		super("OPA");
		
		string gladeFile;
		if (type == to!int(opaDice.black))
			gladeFile = gladePath ~ "opaThrowerWindow.glade";
		else if (type == to!int(opaDice.white))
			gladeFile = gladePath ~ "opaOwnerWindow.glade";
		else
			gladeFile = gladePath ~ "opaThrowerWindow.glade";
			
		this.gladeFile = gladeFile;
		this.board = board;
		this.type = type;
		
		this.throwerName = board.opaInfo["throwerName"];
		this.ownerName = board.opaInfo["ownerName"];
		this.opaShareName = board.shares[to!uint(board.opaInfo["shareId"])].name;
		
		setupUI();
		
		opaDescription.setLabel(throwerName ~ ": OPA ——>" ~ ownerName ~ " (" ~ opaShareName ~ ")");
		
		btnOpaDice.addOnClicked(&actionOpa);
		btnClose.addOnClicked(&submit!Button);
		
 		w.showAll();
	}
	
	void setupUI() {
		Builder b = new Builder();
		b.addFromFile(gladeFile);
		w = cast(Window)b.getObject("window1");
		
		btnOpaDice = cast(Button)b.getObject("btnOpaDice");
		btnClose = cast(Button)b.getObject("btnClose");
		btnClose.setSensitive(true);
		
		opaDescription = cast(Label)b.getObject("opaDescription");
		
		if (this.type == to!int(opaDice.spectator))
			btnOpaDice.setSensitive(false);
		cashWidget = cast(Label)b.getObject("cash");
		//We get the thrower cash amount to print it in the Label
		cashWidget.setLabel(to!string(board.players[throwerName].cash));
		
		throwerFrame = cast(Frame)b.getObject("throwerFrame");
		ownerFrame = cast(Frame)b.getObject("ownerFrame");
		
		labelThrower = cast(Label)b.getObject("labelThrower");
		labelOwner = cast(Label)b.getObject("labelOwner");
		labelThrower.setLabel(throwerName);
		labelOwner.setLabel(ownerName);
	}
	
	void removeDice(Image dice) {
		if (dice !is null)
			dice.destroy();
	}
	
	//opa dice button action
	void actionOpa(Button b) {
		//We ask to mainThread to throw opa Dice of current type
		if (type == to!int(opaDice.black)) {
			ownerTid.send(usrCmdThrowOpaBlack);
			//then we disable the close button (needed only for thrower)
			btnClose.setSensitive(false);
		}
		else if (type == to!int(opaDice.white))
			ownerTid.send(usrCmdThrowOpaWhite);
		btnOpaDice.setSensitive(false);
	}
	
	void submit(T)(T obj) {
		//We send signal to ownerTid that opa has finished
		ownerTid.send(usrCmdEndOpa);
		w.destroy();
	}
}

class AskNOfTokens : MainWindow {
	Window w;
	Tid thread;
	string gladeFile;
	SpinButton spinInput;
	Button close;
	
	this (string gladeFile, Tid thread) {
		super("Number of Tokens");
		this.gladeFile = gladeFile;
		this.thread = thread;
		setupUI();
		w.showAll();
	}
	
	void setupUI() {
		Builder b = new Builder();
		b.addFromFile(gladeFile);
		w = cast(Window)b.getObject("window1");
		spinInput = cast(SpinButton)b.getObject("nTokens");
		close = cast(Button)b.getObject("btnOk");
		close.addOnClicked(&submit!Button);
	}
	
	void submit(T)(T obj) {
		thread.send(to!uint(spinInput.getText()));
		w.destroy();
	}
}

class SharesInfo : MainWindow {
	Window w;
	string gladeFile;
	immutable Share[string] shares;
	Image[] img;
	Label[] info;
	Grid sharesGrid;
	Button sharesOk;
	
	this (string gladeFile, immutable Player player) {
		super("Shares info");
		this.gladeFile = gladeFile;
		setupUI();
		
		this.shares = player.sharesOwned;
		sharesOk.addOnClicked(&submit!Button);
		
		sharesGrid.attach(new Label(player.name), 0, 0, 2, 1);
		
		uint row = 1;
		if (shares.length <= 0)
			sharesGrid.attach(new Label("No shares owned"), 0, 1, 2, 1);
		foreach(shareName, share; shares) {
			//For now we use a Label, soon i will create images with sharename as filename
			auto shareImage = new Label(shareName);
			
			//OPA table
 			auto infoString = format("Price: $%s \nO.P.A. expired: $%s \n%s\t%s\t%s\t%s\t%s\n", share.price, share.expiredOpa, "60%", "80%", "100%", "120%", "140%");
			foreach(percent; share.opaPercentTable) {
				string number = to!string(percent);
				infoString ~= format("$%-" ~ to!string(number.length + 3) ~ "s", percent);
			}
			auto info = new Label(infoString);
			
			sharesGrid.attach(shareImage, 0, row, 1, 1);
			sharesGrid.attach(info, 1, row++, 1, 1);
		}
		w.showAll();
	}
	
	void setupUI() {
		Builder b = new Builder();
		b.addFromFile(gladeFile);
		w = cast(Window)b.getObject("window1");
		sharesGrid = cast(Grid)b.getObject("sharesGrid");
		sharesOk = cast(Button)b.getObject("sharesOk");
	}
	
	void submit(T)(T obj) {
		w.destroy();
	}
}

// ************** Main App *****************
class View : MainWindow
{	
	private {
	
		enum origin {
			left = 7,
			top = 12
		}
		enum boardSize {
			width = 8,
			height = 13
		}
		
		struct boardBox {
			EventBox listener;
			Frame frame;
			Box container;
			
			this(Box container, uint position) {
				this.frame = new Frame("");
				this.container = container;
				this.listener = new EventBox();
				listener.setName(to!string(position));
				frame.add(this.container);
				listener.add(frame);
			}
		}
		
		struct SpecialPopupBox {
			static bool yesNo(string message, string title, 
					string yesButton, string noButton) {
				MessageDialog d = new MessageDialog(
						null, cast(GtkDialogFlags)0,
						MessageType.QUESTION,
						ButtonsType.NONE,
						message
				);
				d.setTitle(title);
				d.addButton(yesButton, ResponseType.YES);
				d.addButton(noButton, ResponseType.NO);
				int responce = d.run();
				d.destroy();
				return responce == ResponseType.YES;
			}
		}
	
		//Application application;
		OpaWindow opa;
		AskNOfTokens askTokens;
		SharesInfo sharesInfo;
		string[] tokenImages;
		/*The reference to board information is immutable otherwise we will violate the MVC pattern.
		  Only the controller and the engine should alter the game logic!*/
		immutable Board board;
		Tid gui;
		Tid eListener;
		Tid chatSenderThread;
		Tid nTalker;
		string myPlayerName;
		
		Builder b;
		Grid grid;
		//Frame[numBoxes] tokenBoxes;
		//Box[numBoxes] tokenBoxes;
		boardBox[numBoxes] tokenBoxes;
		Image[string] token;
		Image[2] uiDices;
		PlayersList listModel;
		TreeView lstPlayers;
		TreeViewColumn icon;
		TreeViewColumn name;
		TreeViewColumn address;
		Button dice;
		Button jwt;
		Button sintex;
		Button shares;
		SpinButton nDices;
		Label msgBox;
		Label upCard;
		Label boxInfo;
		Label nProducts;
		Label nRawMat;
		Label nEnergy;
		Label nComputer;
		Label nJwt;
		Label nSintex;
		Label nMilano;
		Label nCash;
		Label nCredito;
		Label nRoma;
		/*EventBox msgBoxBg;
		EventBox upCardBg;
		EventBox boxInfoBg;*/
		TextView chat;
		Entry msgInputBox;
		Button sendMessage;
		Window w;
		Idle service;
	}//end private
	
	this(immutable Board board, string myPlayerName) {
		this.board = board;
		version(gtk) {
			//super(application);
			super("Manager");
			tokenImages = ["blueToken.png", "redToken.png", "violetToken.png", "yellowToken.png", "greenToken.png", "cyanToken.png"];
			
			this.myPlayerName = myPlayerName;
			
			//assign token images to all players and refresh their status info
			int i;
			foreach(name, player; board.players)
				token[name] = new Image(gladePath ~ tokenImages[i++]);
			
			//Building the model for lstPlayers TreeView
			listModel = new PlayersList(true);
			foreach(player; board.players)
				listModel.addPlayer(player.name, player.addressPort, token[player.name]);
				
			b = new Builder();
			b.addFromFile(gladePath ~ "board.glade");
			
			setupBoard();
			setupPanel();
			setupStyle();
			
			renderAllPlayers(true);
			
			//Setting all objects event handlers
			dice.addOnClicked(&actionThrowDices);
			jwt.addOnClicked(&actionUseJwt);
			sintex.addOnClicked(&actionUseSintex);
			sendMessage.addOnClicked(&actionSendMessage!Button);
 			msgInputBox.addOnActivate(&actionSendMessage!Entry);
 			lstPlayers.addOnRowActivated(&actionSelectPlayer);
 			nDices.addOnValueChanged(&actionSetDiceNum);
 			shares.addOnClicked(&actionShowShares);
			
			w.showAll();
			
			//gui = spawn(&guiThreadAdapter, cast(shared)this);
 			//register("guiThread", gui);
 			eListener = locate("eListenerThread");
 			chatSenderThread = locate("chatSenderThread");
 			nTalker = locate("netTalker");
 			//fireNow is disabled to wait for event loop to start 
 			startGuiEngine(false);
		}
	}
	
	static void guiThread(Socket server, bool iAmDirector) {
		string myName = View.askName();
		ownerTid.send(myName);
		
		View.askStart(server, iAmDirector);
		if (iAmDirector) {
			server.sendAll(plyCmdStart);
			writeln("Sent START command");
		}
		//Waiting from mainThread the board!
		receive((immutable Board roBoard) {
			//View.createGtkViewApp(myName, roBoard);
			string[] argv = null;
			Main.init(argv);
			View screen = new View(roBoard, myName);
			Main.run();
		});
	}
	
	private {
		void checkRequest(int request, string data, Tid thread) {
			switch (request) {
				//guiEngine dispatch requests
				case UI.renderDices :
					int[] dices = deserialize!(int[])(data);
					renderDices(dices);
				break;
				case UI.renderMessage :
					chatMessage(data);
				break;
				case UI.printMessage :
					printMessage(data);
				break;
				case UI.alert :
					boxInfoMessage(data);
				break;
				case UI.enableUserButtons :
					enableUserButtons();
				break;
				case UI.enableDiceButton :
					enableDiceButton();
				break;
				case UI.enableServiceButtons :
					enableServiceButtons();
				break;
				case UI.disableUserButtons :
					disableUserButtons();
				break;
				case UI.disableDiceButton :
					disableDiceButton();
				break;
				case UI.disableServiceButtons :
					disableServiceButtons();
				break;
				case UI.printConjuncture :
					printConjuncture(data);
				break;
				case UI.showPopupMessage :
					PopupBox.information(data, "Information");
				break;
				case UI.refreshPlayerStatus :
					refreshStatus(board.players[data]);
				break;
				case UI.fullRefreshPlayer :
					//NOTE: renderPlayer will refresh position
					renderPlayer(data);
					refreshStatus(board.players[data]);
				break;
				case UI.showPopupAskPath :
					bool input = SpecialPopupBox.yesNo(data, "Scegli", "Produzione", "Distribuzione");
					thread.send(input);
				break;
				case UI.showPopupAskLoan, UI.showPopupAskSellShare, UI.showPopupAskSellProducts, UI.showAskBuyTokens, UI.showPopupAskSellShareFull, UI.showPopupAskPurchaseShare, UI.showPopupAskOpa :
					bool input = PopupBox.yesNo(data, "Choose");
					thread.send(input);
				break;
				case UI.showAskHowManyTokens :
					askTokens = new AskNOfTokens(gladePath ~ "askNTokens.glade", thread);		
				break;
				case UI.showOpaDialog :
					int type = to!int(data);
					opa = new OpaWindow(board, type);
				break;
				
				//OPA dispatch requests
				case opaUI.renderOpaDice :
					//We extract params from data
					string[] params = deserialize!(string[])(data);
					//We get opa dice value
					string value = params[0];
					//We get opa dice type
					int diceType = to!int(params[1]);
					
					writefln("OPA dice: %s \n Type: %s", value, diceType);
					
					string fileName = value;
					if (value == "EXPIRED") {
						if (diceType == to!int(opaDice.white))
							fileName ~= "W";
						else if (diceType == to!int(opaDice.black))
							fileName ~= "B";
					}
					auto diceImage = new Image(gladePath ~ fileName ~ ".png");
					
					if (diceType == to!int(opaDice.black)) {
						//We remove old dice image
						opa.removeDice(opa.blackDice);
						opa.throwerFrame.add(diceImage);
						opa.blackDice = diceImage;
					}
					else if (diceType == to!int(opaDice.white)) {
						//We remove old dice image
						opa.removeDice(opa.whiteDice);
						opa.ownerFrame.add(diceImage);
						opa.whiteDice = diceImage;
					}
						
					opa.w.showAll();
				break;
				case opaUI.disableOpaDice :
					opa.btnOpaDice.setSensitive(false);
				break;
				case opaUI.enableOpaDice :
					if (opa.type != to!int(opaDice.spectator))
						opa.btnOpaDice.setSensitive(true);
				break;
				case opaUI.disableCloseButton :
					opa.btnClose.setSensitive(false);
				break;
				case opaUI.enableCloseButton :
					opa.btnClose.setSensitive(true);
				break;
				case opaUI.refreshThrowerCash :
					opa.cashWidget.setLabel(data);
				break;
				case opaUI.showOpaResult :
					opa.opaDescription.setLabel(data);
				break;
				default:
					writefln("guiEngine: gui request <%s> not handled", request);
			}
		}
		
		//Gui thread idle service to execute incoming requests for graphics manipulation
		bool guiEngine() {
			receiveTimeout(dur!("msecs")( guiSchedulerTimeout ),
					//Gui requests service (UIST: user interface service for threads)
					(int request, string data, Tid thread) {
						writefln("guiEngine: received request %s with data %s from thread %s", request, data, thread);
						checkRequest(request, data, thread);
					},
					(Variant data) {
						writeln("GuiThread: Format request not handled, received: ", data);
					}
			);
			return true;
		}
		
		//Idles the gtk event loop with the guiEngine service
		void startGuiEngine(bool fireNow) {
			service = new Idle(&guiEngine, GPriority.DEFAULT_IDLE, fireNow);
		}
		
		void setupBoard() {
			w = cast(Window)b.getObject("window1");
			grid = cast(Grid)b.getObject("grid");
			
			msgBox = cast(Label)b.getObject("msgBox");
			msgBox.getStyleContext().addClass("Message");
			msgBox.setLineWrap(true);
			//msgBoxBg = cast(EventBox)b.getObject("msgBoxBg");
			//upCardBg = cast(EventBox)b.getObject("upCardBg");
			//boxInfoBg = cast(EventBox)b.getObject("boxInfoBg");
			upCard = cast(Label)b.getObject("upCard");
			upCard.getStyleContext().addClass("Conjuncture");
			upCard.setLineWrap(true);
			
			boxInfo = cast(Label)b.getObject("boxInfo");
			boxInfo.getStyleContext().addClass("Message");
			
			//Initialize and attach gtk boxes to the grid
			uint left, top;
			foreach (j, ref tokenBox; tokenBoxes) {
				//tokenBox = new Box(GtkOrientation.HORIZONTAL, 0);
				//tokenBox = new Frame(to!string(j + 1));
				tokenBox = boardBox(new Box(GtkOrientation.HORIZONTAL, 0), to!uint(j + 1));
				if (j == 0) {
					left = 7;
					top = 12;
				}
				else if (j <= 7)
					--left;
				else if (j <= 19)
					--top;
				else if (j <= choosePath - 1)
					++left;
				else if (j <= 37) {
					left = 7;
					++top;
				}
				else if (j == 38) {
					left = 8;
					top = 1;
				}
				else if (j <= 48)
					++top;
				//grid.attach(new Image(gladePath ~ "boxes/" ~ to!string(j + 1) ~ ".png"), left, top, 1, 1);
				/*auto boxImage = new Image();
				boxImage.setHalign(GtkAlign.CENTER);*/
				//Setting board frames ID for css selectors
				//tokenBox.frame.setName(to!string(j + 1));
				tokenBox.listener.getStyleContext().addClass("Box");		
				tokenBox.listener.addOnButtonPress(&actionShowBoxDetails);
				
				grid.attach(new Image(gladePath ~ "empty.png"), left, top, 1, 1);
				/*if (j == 0) {
					boxImage.setFromFile(gladePath ~ "boxes/" ~ to!string(j + 1) ~ ".png");
					grid.attach(boxImage, left, top, 1, 1);
				}*/
					
				//Conjuncture boxes n.1 and n.27 width is larger (2)	
				uint width = 1;
				if ((j == 0) || (j == (choosePath - 1)))
					width = 2;
				grid.attach(tokenBox.listener, left, top, width, 1);
			}
		}
		
		void setupPanel() {
			//Setup TreeView
			lstPlayers = cast(TreeView)b.getObject("lstPlayers");
			lstPlayers.getSelection().setMode(GtkSelectionMode.BROWSE);
			lstPlayers.setFixedHeightMode(false);
			lstPlayers.setModel(listModel);
			//Forces the first row to be selected
			lstPlayers.getSelection().selectPath(new TreePath("0"));
			
			icon = new TreeViewColumn("Pedina", new CellRendererPixbuf(), "pixbuf", 0);
			lstPlayers.appendColumn(icon);
			
			name = new TreeViewColumn("Nome", new CellRendererText(), "text", 1);
			lstPlayers.appendColumn(name);
			
			address = new TreeViewColumn("Indirizzo", new CellRendererText(), "text", 2);
			lstPlayers.appendColumn(address);
			
			//Panel buttons
			dice = cast(Button)b.getObject("btnDice");
			jwt = cast(Button)b.getObject("btnJwt");
			sintex = cast(Button)b.getObject("btnSintex");
			shares = cast(Button)b.getObject("btnShares");
			
			//Panel status info
			nProducts = cast(Label)b.getObject("nProducts");
			nDices = cast(SpinButton)b.getObject("nDices");
			nRawMat = cast(Label)b.getObject("nRawMat");
			nEnergy = cast(Label)b.getObject("nEnergy");
			nComputer = cast(Label)b.getObject("nComputer");
			nJwt = cast(Label)b.getObject("nJwt");
			nSintex = cast(Label)b.getObject("nSintex");
			nMilano = cast(Label)b.getObject("nMilano");
			nCash = cast(Label)b.getObject("nCash");
			nCredito = cast(Label)b.getObject("nCredito");
			nRoma = cast(Label)b.getObject("nRoma");
			
			//Setup Chat
			chat = cast(TextView)b.getObject("chat");
			msgInputBox = cast(Entry)b.getObject("msgInputBox");
			sendMessage = cast(Button)b.getObject("sendMessage");
			
			//Loads self player's status info
			refreshStatus(board.players[myPlayerName]);
		}
		
		void setupStyle() {
			//import gtkc.gtk;
			auto style = new CssProvider();
			style.loadFromPath(gladePath ~ "style.css");
			Screen globalScreen = Display.getDefault().getDefaultScreen();
			StyleContext.addProviderForScreen(globalScreen, style, GTK_STYLE_PROVIDER_PRIORITY_USER);
			//gtk_style_context_add_provider_for_screen(globalScreen.getScreenStruct(), style.getStyleProviderStruct(), GTK_STYLE_PROVIDER_PRIORITY_USER);
		}
		
		void enableUserButtons() {
			dice.setSensitive(true);
			jwt.setSensitive(true);
			sintex.setSensitive(true);
			nDices.setSensitive(true);
			//shares.setSensitive(true);
		}
		
		void disableUserButtons() {
			dice.setSensitive(false);
			jwt.setSensitive(false);
			sintex.setSensitive(false);
			nDices.setSensitive(false);
			//shares.setSensitive(false);
		}
		
		void disableServiceButtons() {
			jwt.setSensitive(false);
			sintex.setSensitive(false);
		}
		
		void enableServiceButtons() {
			jwt.setSensitive(true);
			sintex.setSensitive(true);
		}
		
		void enableDiceButton() {
			dice.setSensitive(true);
		}
		
		void disableDiceButton() {
			dice.setSensitive(false);
		}
		
		void printMessage(const (char)[] msg) {
			msgBox.setLabel(msg.idup);
			w.showAll();
		}
		
		void boxInfoMessage(const (char)[] msg) {
			boxInfo.setLabel(msg.idup);
			w.showAll();
		}
		
		void printConjuncture(const (char)[] text) {
			upCard.setLabel(text.idup);
			w.showAll();
		}
		
		void refreshPosition(immutable Player player) {
			//Old code
			//tokenBoxes[player.pos.getOld - 1].container.remove(token[player.name]);
			
			//New code. It's better to destroy the widget instead of asking the container to remove it
			auto temp = token[player.name].getPixbuf();
			if (token[player.name] !is null)
				token[player.name].destroy();
			writefln("REMOVING TOKEN FROM POSITION: %s", player.pos.getOld - 1);
			token[player.name] = new Image(temp);
			tokenBoxes[player.pos - 1].container.add(token[player.name]);
			writefln("PUTTING TOKEN TO POSITION: %s", player.pos - 1);
			w.showAll();
		}
		
		void refreshStatus(immutable Player player) {
			nProducts.setLabel(to!string(player.ownedProducts));
			nRawMat.setLabel(to!string(player.products["Raw Materials"]));
			nEnergy.setLabel(to!string(player.products["Energy"]));
			nComputer.setLabel(to!string(player.products["Computers"]));
			nJwt.setLabel(to!string(player.jwt));
			nSintex.setLabel(to!string(player.sintex));
			nMilano.setLabel(to!string(player.insurance));
			nCash.setLabel(to!string(player.cash));
			nCredito.setLabel(to!string(player.loans["Credito Italiano"]));
			nRoma.setLabel(to!string(player.loans["Banco di Roma"]));
		}
		
		/*All action methods are event handlers */
		void actionThrowDices(Button btn) {
			disableUserButtons();
			//Sending the signal event to eventListener thread
			eListener.send("dice");
		}
		
		void actionUseJwt(Button btn) {
			//Sending the signal event to eventListener thread
			eListener.send("jwt");
		}
		
		void actionUseSintex(Button btn) {
			//Sending the signal event to eventListener thread
			eListener.send("sintex");
		}
		
		void actionShowShares(Button btn) {
			string selectedPlayerName = lstPlayers.getSelectedIter().getValueString(1);
			//createGtkDialog!(SharesInfo)(this, gladePath ~ "shares.glade", board.players[selectedPlayerName]);
			sharesInfo = new SharesInfo(gladePath ~ "shares.glade", board.players[selectedPlayerName]);
			Main.run();
		}
		
		void actionSendMessage(T)(T obj) {
			if (msgInputBox.getText() != "") {
				string message = myPlayerName ~ ": " ~ msgInputBox.getText();
				chatMessage(message);
				//Delivers the message to chatSender thread sending all to network
				chatSenderThread.send(message);
			}
		}
		
		void actionSelectPlayer(TreePath tp, TreeViewColumn tvc, TreeView t) {
			string playerName = t.getSelectedIter().getValueString(1);
			refreshStatus(board.players[playerName]);
		}
		
		void actionSetDiceNum(SpinButton s) {
			eListener.send("diceNum " ~ s.getText());
		}
		
		bool actionShowBoxDetails(Event e, Widget clickedBox) {
			string output;
			foreach(name, value; board.boxes[to!uint(clickedBox.getName())])
				output ~= name ~ ": " ~ value ~ "\n";
			
			boxInfo.setLabel(output);
			return true;
		}
		
		void chatMessage(string msg) {
			chat.appendText(msg ~ "\n");
			msgInputBox.setText("");
		}
		
		void renderPlayer(const (char)[] playerName) {
			version(gtk) {
				refreshPosition(board.players[playerName]);
				//refreshStatus(board.players[playerName]);
			}
			foreach(player; board.players)
				writefln("Il giocatore %s si muove da posizione %s a %s", player.name, player.pos.getOld, player.pos());
		}
		
		void renderAllPlayers(bool init = false) {
			version(gtk) {
				foreach(player; board.players) {
					if (!init)
						refreshPosition(player);
					else {
						tokenBoxes[player.pos - 1].container.add(token[player.name]);
					}
				}
			}
			foreach(player; board.players)
				writefln("Il giocatore %s si muove da posizione %s a %s", player.name, player.pos.getOld, player.pos());
		}
		
		void renderDices(int[] dices) {
			version (gtk) {
				writeln("Sto per aggiornare i dadi!");
				if (uiDices[0] !is null)
					uiDices[0].destroy();
				if (uiDices[1] !is null)
					uiDices[1].destroy();
				foreach (i, diceNumber; dices) {
					uiDices[i] = new Image(gladePath ~ to!string(diceNumber) ~ ".png");
					grid.attach(uiDices[i], to!(int)(i + 3), 11, 1, 1);
				}
				w.showAll();
			}
			foreach (dice; dices)
				writeln(dice);
		}
		
		mixin template initGioApp(AppType) {
			import std.random;
			import std.uuid;
			string applicationId = to!string(randomUUID());
			AppType app;
			string applicationName = "src.Viewer." ~ AppType.stringof ~ applicationId;
			Application application = new Application(applicationName, GApplicationFlags.FLAGS_NONE);
		}
	
		/* 
		DataType = data type expected (normally string[])
		AppType = class type to instantiate on activating the application
		*/
		//Used by form only application that does not use parentApp guiEngine
		static DataType createGioApp(AppType, DataType = string[])
					(string gladeFile, Socket server = Socket.init, bool iAmDirector = false)
		{
			mixin initGioApp!AppType;
			
			application.addOnActivate((GioApplication gioApp) {
				static if (AppType.stringof == "WaitingRoomWindow")
					app = new AppType(application, gladeFile, server, iAmDirector);
				else
					app = new AppType(application, gladeFile);
			});
			//Waiting for user submit
			application.run(null);
			static if (AppType.stringof != "WaitingRoomWindow") {
				DataType d = app.getUserData!DataType;
				return d;
			}
		}
		
		//Init info dialog to start the game.
		static Tuple!(string, "address", ushort, "port") askServerAddress(ref string[] argv) {
			version (gtk) {
				string[] userData = createGioApp!AskAddressForm(gladePath ~ "askAddress.glade");
				return Tuple!(string, "address", ushort, "port")(userData[0], to!ushort(userData[1]));
			}
		}
		static string askName() {
			version (gtk) {
				return createGioApp!AskNameForm(gladePath ~ "askName.glade")[0];
			}
		}
		static void askStart(ref Socket server, bool iAmDirector) {
			version(gtk) {
				createGioApp!(WaitingRoomWindow, void)(gladePath ~ "waitingRoomWindow.glade", server, iAmDirector);
			}
		}
		
	}//end private block
	
	
	//Public static methods called by other threads, mainly by ownerTid thread for gui thread requests
	static bool askPath() {
		Tid gui = locate("guiThread");
		gui.sendRequest(UI.showPopupAskPath, questPath);
		/*We can be sure that this receive isnt concurrent with the main one because the main
		receive is blocked inside the throw dice operation*/
		return receiveOnly!bool;
	}
	static bool askLoan(uint percentage, string bankName) {
		Tid gui = locate("guiThread");
		gui.sendRequest(UI.showPopupAskLoan, format(questLoan, percentage, bankName));
		return receiveOnly!bool;
	}
	static bool askSellShare(string shareName, uint earning, uint debt) {
		Tid gui = locate("guiThread");
		gui.sendRequest(UI.showPopupAskSellShare, format(questSellShare, shareName, earning, debt));
		return receiveOnly!bool;
	}
	static bool askSellProducts(uint earning, uint dividend, uint numSalables, string shareHolderName) {
		Tid gui = locate("guiThread");
		string question;
		if (shareHolderName !is null)
			question = format(questSellProducts, numSalables, earning, shareHolderName, dividend);
		else
			question = format(questSellProductsNoShareHolder, numSalables, earning);
		gui.sendRequest(UI.showPopupAskSellProducts, question);
		return receiveOnly!bool;
	}
	static uint askBuyTokens(string tokenType, uint price, uint dividend) {
		Tid gui = locate("guiThread");
		gui.sendRequest(UI.showAskBuyTokens, format(questAskBuyTokens, tokenType, price, dividend));
		bool answer = receiveOnly!bool;
		uint nTokens = 0;
		if (answer) {
			gui.sendRequest(UI.showAskHowManyTokens, questHowManyTokens);
			nTokens = receiveOnly!uint;
		}
		return nTokens;
	}
	static void opaDialog(int diceType) {
		Tid gui = locate("guiThread");
		gui.sendRequest(UI.showOpaDialog, to!string(diceType));
	}
	
	//Debug only methods
	void showBoard() {
		uint boardLength = numBoxes;
		foreach (i; 1 .. boardLength + 1) 
		{
			if (i in board.boxes) {
				foreach (column, info; board.boxes[i])
					writeln(column, ": ", info);
			}
		}
		mixin(debugInfo("DEBUG: prints players in the board"));
		debug writeln(board.players);
	}
	static void showCurrentPlayer(Player player) {
		writeln(player.name);
	}
	
	static void showMessage(const (char)[] message) {
		writeln(message);
	}
}
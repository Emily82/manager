PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE "langs"(
id integer primary key autoincrement,
code varchar(5) not null,
unique(code));
INSERT INTO "langs" VALUES(1,'it');
INSERT INTO "langs" VALUES(2,'en');
CREATE TABLE "banks"(
id integer primary key autoincrement,
name varchar(20) not null);
INSERT INTO "banks" VALUES(1,'Banco di Roma');
INSERT INTO "banks" VALUES(2,'Credito Italiano');
CREATE TABLE shares(
id integer primary key autoincrement,
name varchar(20) not null,
price integer not null,
unique(name));
INSERT INTO "shares" VALUES(1,'Olivetti',30000);
INSERT INTO "shares" VALUES(2,'Jwt',20000);
INSERT INTO "shares" VALUES(3,'Banco di Roma',40000);
INSERT INTO "shares" VALUES(4,'Sintex',20000);
INSERT INTO "shares" VALUES(5,'Alitalia',30000);
INSERT INTO "shares" VALUES(6,'Compagnia di Assicurazione Milano',10000);
INSERT INTO "shares" VALUES(7,'Dalmine',60000);
INSERT INTO "shares" VALUES(8,'Standa',15000);
INSERT INTO "shares" VALUES(9,'Credito Italiano',50000);
INSERT INTO "shares" VALUES(10,'Postal Market',25000);
INSERT INTO "shares" VALUES(11,'Enel',50000);
CREATE TABLE servicetypes(
id integer primary key autoincrement,
share_id integer not null,
unique(share_id),
foreign key(share_id) references shares(id));
INSERT INTO "servicetypes" VALUES(1,2);
INSERT INTO "servicetypes" VALUES(2,4);
INSERT INTO "servicetypes" VALUES(3,6);
CREATE TABLE saletypes(
id integer primary key autoincrement,
share_id integer not null, numsalables integer not null default 1,
unique(share_id),
foreign key(share_id) references shares(id));
INSERT INTO "saletypes" VALUES(1,5,1);
INSERT INTO "saletypes" VALUES(2,10,2);
INSERT INTO "saletypes" VALUES(3,8,3);
CREATE TABLE bankboxes(
id integer primary key autoincrement,
percentage integer not null,
bank_id integer not null,
foreign key(bank_id) references banks(id));
INSERT INTO "bankboxes" VALUES(1,10,1);
INSERT INTO "bankboxes" VALUES(2,20,1);
INSERT INTO "bankboxes" VALUES(3,30,1);
INSERT INTO "bankboxes" VALUES(4,10,2);
INSERT INTO "bankboxes" VALUES(5,20,2);
INSERT INTO "bankboxes" VALUES(6,30,2);
CREATE TABLE serviceboxes (
id integer primary key autoincrement,
price integer not null,
dividend integer not null,
servicetype_id integer not null,
foreign key(servicetype_id) references servicetypes(id));
INSERT INTO "serviceboxes" VALUES(1,10000,5000,1);
INSERT INTO "serviceboxes" VALUES(2,8000,4000,1);
INSERT INTO "serviceboxes" VALUES(3,10000,5000,2);
INSERT INTO "serviceboxes" VALUES(4,8000,4000,2);
INSERT INTO "serviceboxes" VALUES(5,8000,4000,3);
INSERT INTO "serviceboxes" VALUES(6,6000,3000,3);
CREATE TABLE productionboxes(
id integer primary key autoincrement,
price integer not null,
dividend integer not null,
productiontype_id integer not null,
foreign key(productiontype_id) references ProductionTypes(id));
INSERT INTO "productionboxes" VALUES(1,16000,8000,1);
INSERT INTO "productionboxes" VALUES(2,12000,4000,1);
INSERT INTO "productionboxes" VALUES(3,8000,2000,1);
INSERT INTO "productionboxes" VALUES(4,14000,7000,2);
INSERT INTO "productionboxes" VALUES(5,10000,3000,2);
INSERT INTO "productionboxes" VALUES(6,7000,2000,2);
INSERT INTO "productionboxes" VALUES(7,8000,4000,3);
INSERT INTO "productionboxes" VALUES(8,6000,2000,3);
INSERT INTO "productionboxes" VALUES(9,4000,1000,3);
CREATE TABLE shareboxes(
id integer primary key autoincrement,
share_id integer not null,
unique(share_id),
foreign key(share_id) references shares(id));
INSERT INTO "shareboxes" VALUES(1,1);
INSERT INTO "shareboxes" VALUES(2,2);
INSERT INTO "shareboxes" VALUES(3,3);
INSERT INTO "shareboxes" VALUES(4,4);
INSERT INTO "shareboxes" VALUES(5,5);
INSERT INTO "shareboxes" VALUES(6,6);
INSERT INTO "shareboxes" VALUES(7,7);
INSERT INTO "shareboxes" VALUES(8,8);
INSERT INTO "shareboxes" VALUES(9,9);
INSERT INTO "shareboxes" VALUES(10,10);
INSERT INTO "shareboxes" VALUES(11,11);
CREATE TABLE saleboxes(
id integer primary key autoincrement,
price integer not null,
dividend integer not null,
saletype_id integer not null,
foreign key(saletype_id) references saletypes(id));
INSERT INTO "saleboxes" VALUES(1,20000,15000,1);
INSERT INTO "saleboxes" VALUES(2,33000,9000,1);
INSERT INTO "saleboxes" VALUES(3,47000,6000,1);
INSERT INTO "saleboxes" VALUES(4,40000,25000,2);
INSERT INTO "saleboxes" VALUES(5,66000,15000,2);
INSERT INTO "saleboxes" VALUES(6,94000,10000,2);
INSERT INTO "saleboxes" VALUES(7,60000,22000,3);
INSERT INTO "saleboxes" VALUES(8,99000,13000,3);
INSERT INTO "saleboxes" VALUES(9,141000,9000,3);
CREATE TABLE companies (
id integer primary key autoincrement,
name varchar(20) not null);
CREATE TABLE board(
id integer primary key autoincrement,
bankbox_id integer default null,
servicebox_id integer default null,
productionbox_id integer default null,
sharebox_id integer default null,
salebox_id integer default null,
specialbox_id integer default null,
foreign key(bankbox_id) references bankboxes(id),
foreign key(servicebox_id) references serviceboxes(id),
foreign key(productionbox_id) references productionboxes(id),
foreign key(sharebox_id) references shareboxes(id),
foreign key(salebox_id) references saleboxes(id),
foreign key(specialbox_id) references specialboxes(id));
INSERT INTO "board" VALUES(1,NULL,NULL,NULL,NULL,NULL,1);
INSERT INTO "board" VALUES(2,4,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(3,NULL,1,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(4,5,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(5,NULL,5,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(6,NULL,3,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(7,6,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(8,NULL,NULL,NULL,NULL,NULL,2);
INSERT INTO "board" VALUES(9,NULL,NULL,NULL,1,NULL,NULL);
INSERT INTO "board" VALUES(10,NULL,NULL,NULL,2,NULL,NULL);
INSERT INTO "board" VALUES(11,NULL,NULL,NULL,3,NULL,NULL);
INSERT INTO "board" VALUES(12,NULL,NULL,NULL,4,NULL,NULL);
INSERT INTO "board" VALUES(13,NULL,NULL,NULL,5,NULL,NULL);
INSERT INTO "board" VALUES(14,NULL,NULL,NULL,6,NULL,NULL);
INSERT INTO "board" VALUES(15,NULL,NULL,NULL,7,NULL,NULL);
INSERT INTO "board" VALUES(16,NULL,NULL,NULL,8,NULL,NULL);
INSERT INTO "board" VALUES(17,NULL,NULL,NULL,9,NULL,NULL);
INSERT INTO "board" VALUES(18,NULL,NULL,NULL,10,NULL,NULL);
INSERT INTO "board" VALUES(19,NULL,NULL,NULL,11,NULL,NULL);
INSERT INTO "board" VALUES(20,NULL,NULL,NULL,NULL,NULL,4);
INSERT INTO "board" VALUES(21,1,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(22,NULL,2,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(23,2,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(24,NULL,6,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(25,NULL,4,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(26,3,NULL,NULL,NULL,NULL,NULL);
INSERT INTO "board" VALUES(27,NULL,NULL,NULL,NULL,NULL,3);
INSERT INTO "board" VALUES(28,NULL,NULL,1,NULL,NULL,NULL);
INSERT INTO "board" VALUES(29,NULL,NULL,4,NULL,NULL,NULL);
INSERT INTO "board" VALUES(30,NULL,NULL,7,NULL,NULL,NULL);
INSERT INTO "board" VALUES(31,NULL,NULL,NULL,NULL,NULL,5);
INSERT INTO "board" VALUES(32,NULL,NULL,2,NULL,NULL,NULL);
INSERT INTO "board" VALUES(33,NULL,NULL,5,NULL,NULL,NULL);
INSERT INTO "board" VALUES(34,NULL,NULL,8,NULL,NULL,NULL);
INSERT INTO "board" VALUES(35,NULL,NULL,NULL,NULL,NULL,7);
INSERT INTO "board" VALUES(36,NULL,NULL,3,NULL,NULL,NULL);
INSERT INTO "board" VALUES(37,NULL,NULL,6,NULL,NULL,NULL);
INSERT INTO "board" VALUES(38,NULL,NULL,9,NULL,NULL,NULL);
INSERT INTO "board" VALUES(39,NULL,NULL,NULL,NULL,1,NULL);
INSERT INTO "board" VALUES(40,NULL,NULL,NULL,NULL,4,NULL);
INSERT INTO "board" VALUES(41,NULL,NULL,NULL,NULL,7,NULL);
INSERT INTO "board" VALUES(42,NULL,NULL,NULL,NULL,NULL,8);
INSERT INTO "board" VALUES(43,NULL,NULL,NULL,NULL,2,NULL);
INSERT INTO "board" VALUES(44,NULL,NULL,NULL,NULL,5,NULL);
INSERT INTO "board" VALUES(45,NULL,NULL,NULL,NULL,8,NULL);
INSERT INTO "board" VALUES(46,NULL,NULL,NULL,NULL,NULL,6);
INSERT INTO "board" VALUES(47,NULL,NULL,NULL,NULL,3,NULL);
INSERT INTO "board" VALUES(48,NULL,NULL,NULL,NULL,6,NULL);
INSERT INTO "board" VALUES(49,NULL,NULL,NULL,NULL,9,NULL);
CREATE TABLE specialboxes(
id integer primary key autoincrement,
specialtype_id integer not null,
foreign key(specialtype_id) references SpecialTypes(id));
INSERT INTO "specialboxes" VALUES(1,1);
INSERT INTO "specialboxes" VALUES(2,1);
INSERT INTO "specialboxes" VALUES(3,1);
INSERT INTO "specialboxes" VALUES(4,2);
INSERT INTO "specialboxes" VALUES(5,2);
INSERT INTO "specialboxes" VALUES(6,2);
INSERT INTO "specialboxes" VALUES(7,3);
INSERT INTO "specialboxes" VALUES(8,4);
CREATE TABLE productiontypes (
    "id" INTEGER,
    "share_id" INTEGER NOT NULL
, "name" TEXT);
INSERT INTO "productiontypes" VALUES(1,7,NULL);
INSERT INTO "productiontypes" VALUES(2,11,NULL);
INSERT INTO "productiontypes" VALUES(3,1,NULL);
CREATE TABLE businessdeck(
id integer primary key autoincrement,
content_id integer not null,
foreign key(content_id) references contents(id));
INSERT INTO "businessdeck" VALUES(1,38);
INSERT INTO "businessdeck" VALUES(2,39);
INSERT INTO "businessdeck" VALUES(3,40);
INSERT INTO "businessdeck" VALUES(4,41);
INSERT INTO "businessdeck" VALUES(5,42);
INSERT INTO "businessdeck" VALUES(6,43);
INSERT INTO "businessdeck" VALUES(7,44);
INSERT INTO "businessdeck" VALUES(8,45);
INSERT INTO "businessdeck" VALUES(9,46);
INSERT INTO "businessdeck" VALUES(10,47);
INSERT INTO "businessdeck" VALUES(11,48);
INSERT INTO "businessdeck" VALUES(12,49);
INSERT INTO "businessdeck" VALUES(13,50);
INSERT INTO "businessdeck" VALUES(14,51);
INSERT INTO "businessdeck" VALUES(15,52);
INSERT INTO "businessdeck" VALUES(16,53);
INSERT INTO "businessdeck" VALUES(17,54);
INSERT INTO "businessdeck" VALUES(18,55);
INSERT INTO "businessdeck" VALUES(19,56);
INSERT INTO "businessdeck" VALUES(20,57);
INSERT INTO "businessdeck" VALUES(21,58);
INSERT INTO "businessdeck" VALUES(22,59);
INSERT INTO "businessdeck" VALUES(23,60);
INSERT INTO "businessdeck" VALUES(24,61);
INSERT INTO "businessdeck" VALUES(25,62);
INSERT INTO "businessdeck" VALUES(26,63);
INSERT INTO "businessdeck" VALUES(27,64);
INSERT INTO "businessdeck" VALUES(28,65);
INSERT INTO "businessdeck" VALUES(29,66);
INSERT INTO "businessdeck" VALUES(30,67);
INSERT INTO "businessdeck" VALUES(31,68);
INSERT INTO "businessdeck" VALUES(32,69);
INSERT INTO "businessdeck" VALUES(33,70);
CREATE TABLE conjuncturedeck(
id integer primary key autoincrement,
content_id integer not null,
foreign key(content_id) references contents(id));
INSERT INTO "conjuncturedeck" VALUES(1,5);
INSERT INTO "conjuncturedeck" VALUES(2,6);
INSERT INTO "conjuncturedeck" VALUES(3,7);
INSERT INTO "conjuncturedeck" VALUES(4,8);
INSERT INTO "conjuncturedeck" VALUES(5,9);
INSERT INTO "conjuncturedeck" VALUES(6,10);
INSERT INTO "conjuncturedeck" VALUES(7,11);
INSERT INTO "conjuncturedeck" VALUES(8,12);
INSERT INTO "conjuncturedeck" VALUES(9,13);
INSERT INTO "conjuncturedeck" VALUES(10,14);
INSERT INTO "conjuncturedeck" VALUES(11,15);
INSERT INTO "conjuncturedeck" VALUES(12,16);
INSERT INTO "conjuncturedeck" VALUES(13,17);
INSERT INTO "conjuncturedeck" VALUES(14,18);
INSERT INTO "conjuncturedeck" VALUES(15,19);
INSERT INTO "conjuncturedeck" VALUES(16,20);
INSERT INTO "conjuncturedeck" VALUES(17,21);
INSERT INTO "conjuncturedeck" VALUES(18,22);
INSERT INTO "conjuncturedeck" VALUES(19,23);
INSERT INTO "conjuncturedeck" VALUES(20,24);
INSERT INTO "conjuncturedeck" VALUES(21,25);
INSERT INTO "conjuncturedeck" VALUES(22,26);
INSERT INTO "conjuncturedeck" VALUES(23,27);
INSERT INTO "conjuncturedeck" VALUES(24,28);
INSERT INTO "conjuncturedeck" VALUES(25,29);
INSERT INTO "conjuncturedeck" VALUES(26,30);
INSERT INTO "conjuncturedeck" VALUES(27,31);
INSERT INTO "conjuncturedeck" VALUES(28,32);
INSERT INTO "conjuncturedeck" VALUES(29,33);
INSERT INTO "conjuncturedeck" VALUES(30,34);
INSERT INTO "conjuncturedeck" VALUES(31,35);
INSERT INTO "conjuncturedeck" VALUES(32,36);
INSERT INTO "conjuncturedeck" VALUES(33,37);
CREATE TABLE specialtypes(
id integer primary key autoincrement,
content_id integer not null,
foreign key(content_id) references contents(id));
INSERT INTO "specialtypes" VALUES(1,1);
INSERT INTO "specialtypes" VALUES(2,2);
INSERT INTO "specialtypes" VALUES(3,3);
INSERT INTO "specialtypes" VALUES(4,4);
CREATE TABLE contents (
id integer primary key autoincrement,
text text not null,
lang_id integer not null,
foreign key(lang_id) references langs(id));
INSERT INTO "contents" VALUES(1,'Congiuntura',1);
INSERT INTO "contents" VALUES(2,'Vita degli Affari',1);
INSERT INTO "contents" VALUES(3,'Esplosione con distruzione di materie prime',1);
INSERT INTO "contents" VALUES(4,'Incendio nei vostri magazzini',1);
INSERT INTO "contents" VALUES(5,'Congiuntura: Potete, come sempre, continuare a prendere in prestito denaro, ma tutti i rimborsi dei prestiti, in corso o a venire, sono sospesi.',1);
INSERT INTO "contents" VALUES(6,'Congiuntura: Non potete più contrarre prestiti al 10%%.',1);
INSERT INTO "contents" VALUES(7,'Congiuntura: Se possedete un''azione di una Società di distribuzione, non riceverete dal cassiere nessun dividendo quando venderete prodotti finiti.',1);
INSERT INTO "contents" VALUES(8,'Congiuntura: Ogni giocatore , al suo turno, deve versare 2000$ al cassiere.',1);
INSERT INTO "contents" VALUES(9,'Congiuntura: Prima di tirare i dadi, dichiarate su quale Società posseduta dai vostri concorrenti intendete lanciare l''O.P.A. Poi, tentate l''O.P.A. -se desiderate - restando sulla casella dove siete.',1);
INSERT INTO "contents" VALUES(10,'Congiuntura: Il prezzo del gettone Sintex è ribassato a 7000$. Però il proprietario dell''azione Sintex non subirà nessuna diminuzione nel suo dividendo.',1);
INSERT INTO "contents" VALUES(11,'Congiuntura: Il prezzo del gettone Jwt è ora di 7000$.',1);
INSERT INTO "contents" VALUES(12,'Congiuntura: Tutti i prestiti che contrarrete avranno un tasso del 30%%.',1);
INSERT INTO "contents" VALUES(13,'Congiuntura: Potete ricevere in prestito 50000$ al 10%% presso una sola banca con la quale non abbiate un prestito in corso. Potete contrarre il prestito al vostro turno e prima di tirare i dadi, rimanendo sulla casella dove siete.',1);
INSERT INTO "contents" VALUES(14,'Congiuntura: Ogni giocatore al suo turno e prima di tirare i dadi, può acquistare quanti simboli energia, desidera al prezzo di 10000$ per ogni simbolo.',1);
INSERT INTO "contents" VALUES(15,'Congiuntura: Non potete acquistarepiù di 2 simboli energia per volta.',1);
INSERT INTO "contents" VALUES(16,'Congiuntura: Ogni giocatore, al suo turno, riceve dal cassiere un rimborso di 3000$.',1);
INSERT INTO "contents" VALUES(17,'Congiuntura: Se possedere l''azione di una Società dei servizi, i benefici che riceverete non saranno da voi incamerati, ma dovranno essere versati al cassiere.',1);
INSERT INTO "contents" VALUES(18,'Congiuntura:Non potete più acquistare simboli materie prime sulla casella dove Dalmine li vende a 8000$.',1);
INSERT INTO "contents" VALUES(19,'Congiuntura: Il prezzo del gettone Sintex è salito a 13000$. Però, il proprietario dell''azione Sintex non ricaverà nessun aumento nel suo dividendo.',1);
INSERT INTO "contents" VALUES(20,'Congiuntura: Ogni giocatore che acquistasimboli materie prime, riceverà dal cassiere un premio di 1000$ per ogni simbolo acquistato.',1);
INSERT INTO "contents" VALUES(21,'Congiuntura: Non potete più vendere i vostri prodotti finiti sulle caselle dove si vendono ai prezzi più alti (3 ultime caselle).',1);
INSERT INTO "contents" VALUES(22,'Congiuntura: Non potete più vendere prodotti finiti.',1);
INSERT INTO "contents" VALUES(23,'Congiuntura: Se possedere l''azione di una Società dei servizi, i dividendi che vi versa il cassiere, quando effettuate transazioni, sono aumentati di 1000$.',1);
INSERT INTO "contents" VALUES(24,'Congiuntura: Non potete acquistare più di 2 simboli informatica alla volta.',1);
INSERT INTO "contents" VALUES(25,'Congiuntura: Se possedete l''azione Dalmine, i dividendi che dovrete incassare in seguito ad acquisti di materie prime non li potete incamerare, ma dovrete versarli al cassiere.',1);
INSERT INTO "contents" VALUES(26,'Congiuntura: Non potete più vendere i vostri prodotti finiti sulle caselle dove si vendono a prezzi più alti (3 ultime caselle).',1);
INSERT INTO "contents" VALUES(27,'Congiuntura: Se possedete l''azione Dalmine, i dividendi che vi versa il cassiere quando vendete simboli materie prime, sono aumentati di 1000$ per ogni simbolo venduto.',1);
INSERT INTO "contents" VALUES(28,'Congiuntura: Non potete più vendere i vostri prodotti finiti sulle caselle dove si vendono ai prezzi più alti (3 ultime caselle).',1);
INSERT INTO "contents" VALUES(29,'Congiuntura: Se vi trovate su una casella del percorso distribuzione (salvo la casella ''incendio nei vostri magazzini'' e ''Vita degli affari''), rimanete bloccato su questa casella e non potete effettuare alcuna operazione.',1);
INSERT INTO "contents" VALUES(30,'Congiuntura: Non potete acquistare più di 2 simboli materie prime alla volta.',1);
INSERT INTO "contents" VALUES(31,'Congiuntura: Non potete più acquistare simboli materie prime.',1);
INSERT INTO "contents" VALUES(32,'Congiuntura: Il prezzo del gettone Jwt è ora di 13000$.',1);
INSERT INTO "contents" VALUES(33,'Congiuntura: Ogni giocatore. al suo turno e prima di tirare i dadi, pesca una carta dal mazzo ''Vita degli affari''.',1);
INSERT INTO "contents" VALUES(34,'Congiuntura: Ogni giocatore può, al suo turno, andare con la sua pedina su una delle 3 caselle centrali del percorso distribuzione, dove i prodotti si vendono al loro prezzo medio. Sarà quindi obbligato a vendere secondo le modalità stabilite dalla relativa casella.',1);
INSERT INTO "contents" VALUES(35,'Congiuntura: Non potete realizzare alcuna operazione sulle azioni che possedete, nè acquisto, nè vendita, nè O.P.A., a meno che non peschiate una carta ''Vita degli affari'' che ve lo permetta.',1);
INSERT INTO "contents" VALUES(36,'Congiuntura: Non potete acquistare più di 2 simboli energia alla volta.',1);
INSERT INTO "contents" VALUES(37,'Congiuntura: Se possedete un''azione di una Società di distribuzione, l''importo che voi riceverete dal cassiere, quando si vendono prodotti finiti, sarà aumentato di 5000$.',1);
INSERT INTO "contents" VALUES(38,'Vita degli Affari: Se possedete 5 o più simboli materie prime, versate al cassiere $5000 per ogni simbolo posseduto.',1);
INSERT INTO "contents" VALUES(39,'Vita degli Affari: Potete acquistare da un vostro concorrente (a vostra scelta) un simbolo informatica e un simbolo materie prime al prezzo globale di $9000. Il concorrente che possiede i suddetti simboli non potrà rifiutarsi.',1);
INSERT INTO "contents" VALUES(40,'Vita degli Affari: Se ne avete, restituite al cassiere un simbolo energia.',1);
INSERT INTO "contents" VALUES(41,'Vita degli Affari: Pagate al cassiere $1000 per ogni simbolo energia posseduto.',1);
INSERT INTO "contents" VALUES(42,'Vita degli Affari: Pagate al cassiere $2000 per ogni simbolo materie prime posseduto.',1);
INSERT INTO "contents" VALUES(43,'Vita degli Affari: Se possedete simboli materie prime, restituitene uno al cassiere e ricevete in cambio $5000.',1);
INSERT INTO "contents" VALUES(44,'Vita degli Affari: Pagate al cassiere $1000 per ogni gettone assicurazione posseduto.',1);
INSERT INTO "contents" VALUES(45,'Vita degli Affari: Se possedete uno o più simboli energia, ricevete dal cassiere $4000.',1);
INSERT INTO "contents" VALUES(46,'Vita degli Affari: Restituite al cassiere, se ne avete, un gettone Sintex.',1);
INSERT INTO "contents" VALUES(47,'Vita degli Affari: Se possedete più simboli materie prime e meno simboli informatica, potete restare sulla casella dove siete arrivati e acquistare dal cassiere tanti simboli informatica quanti volete, al prezzo di $2000 ciascuno.',1);
INSERT INTO "contents" VALUES(48,'Vita degli Affari: Incassate dal cassiere $1000 per ogni gettone assicurazione posseduto.',1);
INSERT INTO "contents" VALUES(49,'Vita degli Affari: Se possedete 2 o più prodotti finiti, restituitene uno al cassiere che ve lo pagherà $15000.',1);
INSERT INTO "contents" VALUES(50,'Vita degli Affari: Pagate al cassiere $4000 per ogni prodotto finito che possedete.',1);
INSERT INTO "contents" VALUES(51,'Vita degli Affari: Se non avete alcun prestito in corso, potete prendere in prestito $50000, al tasso preferenziale del 10%, da una delle 2 banche, a vostra scelta.',1);
INSERT INTO "contents" VALUES(52,'Vita degli Affari: Ricevete dal cassiere $3000 per ogni prodotto finito posseduto.',1);
INSERT INTO "contents" VALUES(53,'Vita degli Affari: Se avete 5 o più simboli informatica, versate al cassiere $2000 per ogni simbolo posseduto.',1);
INSERT INTO "contents" VALUES(54,'Vita degli Affari: Se possedete l''azione Dalmine e se il primo giocatore (seguendo il turno), vuole acquistarla, sarete obbligato a vendergliela, incassando l''80% del suo valore d''acquisto.',1);
INSERT INTO "contents" VALUES(55,'Vita degli Affari: Restando sulla casella dove siete arrivato, potete - se ne possedete - vendere al cassiere un prodotto finito, incassando $42000. Nessun proprietario di Società di distribuzione realizza un beneficio su questa vendita.',1);
INSERT INTO "contents" VALUES(56,'Vita degli Affari: Ricevete gratis dal cassiere 1 simbolo energia.',1);
INSERT INTO "contents" VALUES(57,'Vita degli Affari: Se possedete almeno un prodotto finito, vendetene uno per $25000 al primo giocatore, seguendo il turno, che desidera acquistarlo.',1);
INSERT INTO "contents" VALUES(58,'Vita degli Affari: Restando sulla casella dove siete arrivato, potete acquistare dal cassiere un simbolo materie prime al prezzodi $5000',1);
INSERT INTO "contents" VALUES(59,'Vita degli Affari: Se voi possedete azioni, ma NON quella di DALMINE, potrete cambiarne una (a vostra scelta), con quella di DALMINE. Questa operazione potrà essere fatta sia col cassiere, sia col giocatore che possiede l''azione DALMINE e che sarà obbligato a cedervela.',1);
INSERT INTO "contents" VALUES(60,'Vita degli Affari: Se non possedete azioni del settore di distribuzione e uno dei vostri concorrenti ne possiede, potete costringerlo a vendervene una (a vostra scelta), pagando il 60% del suo valore.',1);
INSERT INTO "contents" VALUES(61,'Vita degli Affari: Se avete avuto un prestito da 2 banche, dovete rimborsarne subito uno a vostra scelta.',1);
INSERT INTO "contents" VALUES(62,'Vita degli Affari: Ricevete dal cassiere $1000 per ogni simbolo informatica posseduto.',1);
INSERT INTO "contents" VALUES(63,'Vita degli Affari: Pagate al cassiere $1000 per ogni simbolo energia che possedete.',1);
INSERT INTO "contents" VALUES(64,'Vita degli Affari: Se possedete almeno un gettone JWT, potete (restando sulla casella dove siete arrivati) acquistarne un altro dal cassiere, al prezzo ridotto di $5000.',1);
INSERT INTO "contents" VALUES(65,'Vita degli Affari: Se possedete 5 o più simboli energia, versate al cassiere $3000 per ogni simbolo posseduto.',1);
INSERT INTO "contents" VALUES(66,'Vita degli Affari: Se possedete più di un''azione del settore distribuzione, dovete rivenderne una (a vostra scelta) al cassiere, incassando l''80% del suo valore d''acquisto.',1);
INSERT INTO "contents" VALUES(67,'Vita degli Affari: Se possedete un''azione del settore dei servizi e anche uno dei vostri concorrenti ne possiede, potete acquistargliene una a vostra scelta, pagando l''80% del suo valore. Il vostro concorrente non potrà rifiutarsi.',1);
INSERT INTO "contents" VALUES(68,'Vita degli Affari: Se possedete più di un''azione del settore dei servizi, dovete rivenderne una al cassiere (a vostra scelta), incassando l''80% del suo valore di acquisto.',1);
INSERT INTO "contents" VALUES(69,'Vita degli Affari: Se possedete 2 o più simboli informatica, versate $5000 al cassiere.',1);
INSERT INTO "contents" VALUES(70,'Vita degli Affari: Scegliete uno dei vostri concorrenti che possiede un prodotto finito. Voi potete comprarglielo, pagando $22000. Il vostro concorrente non potrà rifiutarsi.',1);
DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('langs',2);
INSERT INTO "sqlite_sequence" VALUES('shares',11);
INSERT INTO "sqlite_sequence" VALUES('banks',2);
INSERT INTO "sqlite_sequence" VALUES('bankboxes',6);
INSERT INTO "sqlite_sequence" VALUES('productionboxes',9);
INSERT INTO "sqlite_sequence" VALUES('saletypes',3);
INSERT INTO "sqlite_sequence" VALUES('saleboxes',9);
INSERT INTO "sqlite_sequence" VALUES('servicetypes',3);
INSERT INTO "sqlite_sequence" VALUES('serviceboxes',6);
INSERT INTO "sqlite_sequence" VALUES('shareboxes',11);
INSERT INTO "sqlite_sequence" VALUES('specialboxes',8);
INSERT INTO "sqlite_sequence" VALUES('board',48);
INSERT INTO "sqlite_sequence" VALUES('specialtypes',4);
INSERT INTO "sqlite_sequence" VALUES('contents',70);
INSERT INTO "sqlite_sequence" VALUES('conjuncturedeck',33);
INSERT INTO "sqlite_sequence" VALUES('businessdeck',33);
CREATE UNIQUE INDEX unique_numsalables on saletypes(numsalables);
COMMIT;

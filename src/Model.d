module Model;
import std.stdio;
import std.string;
import std.exception;
import std.format;
import std.algorithm;
import etc.c.sqlite3;
import std.conv;
import std.random;
import std.array;
import gtk.ListStore;
import gtk.TreeIter;
import gtk.Image;
import gdkpixbuf.Pixbuf;
import gtkc.gobjecttypes;
import gobject.Value;
import jsonizer;
import Debug;
import Engine;
import Controller;

enum numBoxes = 49;

enum conjunctureFlags {
	noReturnCashLoan = 1,
	noTenPercentLoan
}

enum businessId {
	fiveRawMaterialsPay = 1,
	buyFromPlayerComputerRawMat,
	
}

enum accidentBoxType {
	rawMaterials = 35,
	products = 42
}

mixin template CardProcedures(alias type) {
	static if (type == "business") {
		void delegate()[uint] businessProcedures = [
			businessId.fiveRawMaterialsPay : delegate() {
								string notifyMessage;
								if (this.products["Raw Materials"] >= 5) {
									pay(5000, banker);
									notifyMessage = format(msgPayed, this.name, 5000);
									server.sendAll(plyCmdNotify ~ notifyMessage);
								}
							 },
		        businessId.buyFromPlayerComputerRawMat : delegate() {
								
							 },
			
		];
	}
	else static if (type == "conjuncture") {
		void delegate()[uint] conjunctureProcedures = [
			conjunctureFlags.noReturnCashLoan : delegate() {
								writeln("test conjuncture 1");
							    },
			conjunctureFlags.noTenPercentLoan : delegate() {
								writeln("test conjuncture 2");
							    }
	
		];
	}
}

//Database models
class Database {
	private Table[string] tables;
	string[] expectedTables = ["bankboxes", "langs", "saletypes", "sqlite_sequence" , "banks", 
				   "productionboxes", "serviceboxes", "specialtypes", "businessdeck",
				   "productiontypes", "servicetypes", "companies", "shareboxes", 
				   "conjuncturedeck", "saleboxes", "shares", "board", "specialboxes", 
				   "contents"];
	string[] foundTables;

	this (const (char)[] dbFile) {
		Sqlite.dbConnect(dbFile, SQLITE_OPEN_READWRITE);
		
		string[] tableNames = Sqlite.sqlSelect("sqlite_master", "name", "type='table'").data;
		foundTables = tableNames.dup;
		mixin(debugInfo("DEBUG: mostra i nomi delle tabelle nel database"));
		debug writeln(tableNames);
		
		foreach (name; tableNames)
			tables[name] = new Table(name);
	}
	
	Table getTable(const (char)[] tableName) {
		return (tableName in tables)? tables[tableName]: null;
	}
	
	void sanityCheck() {
		foreach(expectedTable; expectedTables) {
			assert(canFind(foundTables, expectedTable), format(assDbMissingTable, expectedTable));
		}
	}
}

class Table {
	string name;
	string[string][uint] rows;
	uint numCols;
	string[] columnNames;
	
	debug {
		void toString(void delegate(const(char)[]) d) const {
			foreach (name; columnNames)
				formattedWrite(d, "%s\t", name);
			formattedWrite(d, "\n");
			foreach(row; rows) {
				foreach (name; columnNames)
					formattedWrite(d, "%s\t", row[name]);
				formattedWrite(d, "\n");
			}
		}
	}
	
	this (string tableName) {
		name = tableName;
		auto infoData = Sqlite.sqlSelect(name, "*");
		this.columnNames = infoData.columnNames;
		numCols = to!uint(infoData.columnNames.length);
		foreach (r; 0 .. (to!uint(infoData.data.length) / numCols)) {
			foreach (c, name; infoData.columnNames)
				rows[r][name] = infoData.data[(numCols * r) + c];
		}
		mixin(debugInfo("DEBUG: mostra i dati della tabella in una struttura ad alto livello"));
		debug writeln(rows);
	}
}

struct conjunctureCard {
	uint conjunctureFlag;
	string text;
	
	void toString(void delegate(const(char)[]) d) const {
		formattedWrite(d, "[flag: %s, text: %s]", conjunctureFlag, text);
	}
}

class ConjunctureDeck {
	conjunctureCard[] cards;
	conjunctureCard upCard;
	uint[] sequence;
	
	this() {
		foreach(i; 0 .. numConjunctureCards)
			sequence ~= i;
		string columns = "conjuncturedeck.id, contents.text";
		string tables = "conjuncturedeck, contents";
		string where = "lang_id=1 and contents.id = conjuncturedeck.content_id";
		
		auto infoData = Sqlite.sqlSelect(tables, columns, where);
		for (int i = 0 ; i < infoData.data.length; i += infoData.columnNames.length) {
			uint id = to!uint(infoData.data[i]);
			string text = infoData.data[i + 1];
			cards ~= conjunctureCard(id, text);
		}

	}
	
	void takeOneCard() {
		if (!sequence.empty) {
			conjunctureCard card = cards[sequence.front];
			sequence.popFront();
			this.upCard = card;
		}
		//writeln("Sequenza: ", this.sequence);
	}
	
	void shuffle(uint[] newSequence) {
		this.sequence = newSequence;
	}
}

struct businessCard {
	uint id;
	string text;
	
	void toString(void delegate(const(char)[]) d) const {
		formattedWrite(d, "[id: %s, text: %s]", id, text);
	}
	
	@property bool isEmpty() {
		return ((id == 0)&&(text == ""));
	}
}

class BusinessDeck {
	businessCard[] cards;
	uint[] sequence;
	
	this() {
		foreach(i; 0 .. numBusinessCards)
			sequence ~= i;
		string columns = "businessdeck.id, contents.text";
		string tables = "businessdeck, contents";
		string where = "lang_id=1 and contents.id = businessdeck.content_id";
		
		auto infoData = Sqlite.sqlSelect(tables, columns, where);
		for (int i = 0 ; i < infoData.data.length; i += infoData.columnNames.length) {
			uint id = to!uint(infoData.data[i]);
			string text = infoData.data[i + 1];
			cards ~= businessCard(id, text);
		}
	}
	
	businessCard takeOneCard() {
		if (!sequence.empty) {
			businessCard card = cards[sequence.front];
			sequence.popFront();
			return card;
		}
		else
			return businessCard.init;
	}
	
	void shuffle(uint[] newSequence) {
		this.sequence = newSequence;
	}
} 

class Share {
	mixin JsonizeMe;
	@jsonize {
		uint id;
		string name;
		uint price;
		this (uint id, string name, uint price) {
			this.id = id;
			this.name = name;
			this.price = price;
		}
		
		@property immutable uint expiredOpa() {
			return (price / 5); //20% price
		}
		
		@property immutable uint[] opaPercentTable() {
			return [(price * 3) / 5, (price * 4) / 5, price, (price * 6) / 5, (price * 7) / 5];
		}
	}
	
	void toString(void delegate(const (char)[]) d) {
		formattedWrite(d, "%s: %s: %s", id, name, price);
	}
}

class Board {
	string[string][uint] boxes;
	void delegate(string)[numBoxes + 1] actionRoutines;
	
	Player[string] players;
	
	ConjunctureDeck conjuncture;
	BusinessDeck business;
	
	//We have a copy of all shares also in the board
	immutable Share[uint] shares;
	string[string] opaInfo;
	bool opaRunning;
	
	this(immutable Share[uint] shares, Player[] players) 
	out {
		assert(boxes.length == numBoxes, format(assMissingBoxes, boxes.length, numBoxes));
	}
	body {
		this.shares = shares;
		this.opaRunning = false;
		//Building players associative array
		foreach (player; players) {
			this.players[player.name] = player;
			this.players[player.name].setBoard(this);
		}
			
		conjuncture = new ConjunctureDeck();
		business = new BusinessDeck();
		//Inserimento caselle speciali
		string columns = "board.id, contents.text";
		string tables = "board, specialboxes, specialtypes, contents";
		string where = "board.specialbox_id = specialboxes.id and specialtypes.id = specialboxes.specialtype_id and specialtypes.content_id = contents.id and contents.lang_id = 1";
		auto infoData = Sqlite.sqlSelect(tables, columns, where);
		fillBoard(infoData.columnNames, infoData.data);
		
		//Inserimento caselle prestiti
		columns = "board.id, bankboxes.percentage, banks.name, banks.id as bank_id";
		tables = "board, bankboxes, banks";
		where = "board.bankbox_id = bankboxes.id and bankboxes.bank_id = banks.id;";
		infoData = Sqlite.sqlSelect(tables, columns, where);
		fillBoard(infoData.columnNames, infoData.data);
		
		//Inserimento caselle dei servizi
		columns = "board.id, shares.name, serviceboxes.price, serviceboxes.dividend, shares.id as share_id";
		tables = "board, serviceboxes,servicetypes,shares";
		where = "board.servicebox_id = serviceboxes.id and serviceboxes.servicetype_id = servicetypes.id and servicetypes.share_id = shares.id;";
		infoData = Sqlite.sqlSelect(tables, columns, where);
		fillBoard(infoData.columnNames, infoData.data);
		
		//Inserimento caselle azioni
		columns = "board.id, shares.name, shares.price, shares.id as share_id";
		tables = "board, shareboxes,shares";
		where = "board.sharebox_id = shareboxes.id and shareboxes.share_id = shares.id;";
		infoData = Sqlite.sqlSelect(tables, columns, where);
		fillBoard(infoData.columnNames, infoData.data);
		
		//Inserimento caselle produzione
		columns = "board.id, shares.name, productionboxes.price as price, productionboxes.dividend as dividend";
		tables = "board, productionboxes,productiontypes,shares";
		where = "board.productionbox_id = productionboxes.id and productionboxes.productiontype_id = productiontypes.id and productiontypes.share_id = shares.id;";
		infoData = Sqlite.sqlSelect(tables, columns, where);
		fillBoard(infoData.columnNames, infoData.data);
		
		//Inserimento caselle distribuzione
		columns = "board.id, shares.name, saleboxes.price as sale_price, saleboxes.dividend, saletypes.numsalables";
		tables = "board, saleboxes,saletypes,shares";
		where = "board.salebox_id = saleboxes.id and saleboxes.saletype_id = saletypes.id and saletypes.share_id = shares.id;";
		infoData = Sqlite.sqlSelect(tables, columns, where);
		fillBoard(infoData.columnNames, infoData.data);
		
		mixin(debugInfo("DEBUG: stampa delle caselle inserite nella board"));
		debug writeln(boxes);
		
		//We define a reference to this class so delegates can access all its fields
		Board board = this;
		//BOX ACTION ROUTINES
		//SETTING ROUTINES FOR ALL BOXES
		/+foreach (boxId; 1 .. (numBoxes + 1)) {
			if ((boxId == 1)||(boxId == 8)||(boxId == choosePath)) {
				//Conjuncture routine
				board.actionRoutines[boxId] = (string myPlayerName) {
					board.players[myPlayerName].takeConjuncture();
				};
			}
			else if ((boxId == 20)||(boxId == 31)||(boxId == 46)) {
				//Business routine
				board.actionRoutines[boxId] = (string myPlayerName) {
					//Business card
					board.players[myPlayerName].takeBusinessCard();
				};
			}
			else if ((boxId == 2)||(boxId == 4)||(boxId == 7)||
				 (boxId == 21)||(boxId == 23)||(boxId == 26)) {
				//Banks loans routine
				board.actionRoutines[boxId] = (string myPlayerName) {
					board.players[myPlayerName].takeLoan(board.boxes[boxId]);
				};
			}
			else if ((boxId == 3)||(boxId == 5)||(boxId == 6)||
				 (boxId == 22)||(boxId == 24)||(boxId == 25)||
				 (boxId == 28)||(boxId == 29)||(boxId == 30)||
				 (boxId == 32)||(boxId == 33)||(boxId == 34)||
				 (boxId == 36)||(boxId == 37)||(boxId == 38)) {
				//Buy service and production routine
				board.actionRoutines[boxId] = (string myPlayerName) {
					board.players[myPlayerName].buyTokens(board.boxes[boxId]);
				};
			}
			else if ((boxId >= 9)&&(boxId <= 19)) {
				//Shares routines
				board.actionRoutines[boxId] = (string myPlayerName) {
					board.players[myPlayerName].newShare(board.boxes[boxId]);
				};
			}
		}+/
		
		//Now we build boxes action actionRoutines
		actionRoutines[1] = (string myPlayerName) {
			//Congiuntura
			board.players[myPlayerName].takeConjuncture();
		};
		actionRoutines[2] = (string myPlayerName) {
			//Prestito 10% Credito Italiano
			board.players[myPlayerName].takeLoan(board.boxes[2]);
		};
		actionRoutines[3] = (string myPlayerName) {
			//gettoni JWT
			board.players[myPlayerName].buyTokens(board.boxes[3]);
		};
		actionRoutines[4] = (string myPlayerName) {
			//Prestito 20% Credito Italiano
			board.players[myPlayerName].takeLoan(board.boxes[4]);
		};
		actionRoutines[5] = (string myPlayerName) {
			//Gettoni Assicurazione Milano
			board.players[myPlayerName].buyTokens(board.boxes[5]);
		};
		actionRoutines[6] = (string myPlayerName) {
			//Gettoni sintex
			board.players[myPlayerName].buyTokens(board.boxes[6]);
		};
		actionRoutines[7] = (string myPlayerName) {
			//Prestito 30% Credito Italiano
			board.players[myPlayerName].takeLoan(board.boxes[7]);
		};
		actionRoutines[8] = (string myPlayerName) {
			//Congiuntura
			board.players[myPlayerName].takeConjuncture();
		};
		//Share boxes actionRoutines
		actionRoutines[9] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[9]);
		};
		actionRoutines[10] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[10]);
		};
		actionRoutines[11] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[11]);
		};
		actionRoutines[12] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[12]);
		};
		actionRoutines[13] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[13]);
		};
		actionRoutines[14] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[14]);
		};
		actionRoutines[15] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[15]);
		};
		actionRoutines[16] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[16]);
		};
		actionRoutines[17] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[17]);
		};
		actionRoutines[18] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[18]);
		};
		actionRoutines[19] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[19]);
		};
		/+foreach (boxId; 9 .. 20){
			actionRoutines[boxId] = (string myPlayerName) {
				board.players[myPlayerName].newShare(board.boxes[boxId]);
			};
		}+/
		actionRoutines[20] = (string myPlayerName) {
			//Business card
			board.players[myPlayerName].takeBusinessCard();
		};
		actionRoutines[21] = (string myPlayerName) {
			//Prestito 10% Banco di Roma
			board.players[myPlayerName].takeLoan(board.boxes[21]);
		};
		actionRoutines[22] = (string myPlayerName) {
			//gettoni JWT
			board.players[myPlayerName].buyTokens(board.boxes[22]);
		};
		actionRoutines[23] = (string myPlayerName) {
			//Prestito 20% Banco di Roma
			board.players[myPlayerName].takeLoan(board.boxes[23]);
		};
		actionRoutines[24] = (string myPlayerName) {
			//gettoni Assicurazione Milano
			board.players[myPlayerName].buyTokens(board.boxes[24]);
		};
		actionRoutines[25] = (string myPlayerName) {
			//gettoni sintex
			board.players[myPlayerName].buyTokens(board.boxes[25]);
		};
		actionRoutines[26] = (string myPlayerName) {
			//Prestito 30% Banco di Roma
 			board.players[myPlayerName].takeLoan(board.boxes[26]);
		};
		actionRoutines[27] = (string myPlayerName) {
			//Congiuntura
			board.players[myPlayerName].takeConjuncture();
		};
		actionRoutines[28] = (string myPlayerName) {
			//Dalmine
			board.players[myPlayerName].buyTokens(board.boxes[28]);
		};
		actionRoutines[29] = (string myPlayerName) {
			//Enel
			board.players[myPlayerName].buyTokens(board.boxes[29]);
		};
		actionRoutines[30] = (string myPlayerName) {
			//Olivetti
			board.players[myPlayerName].buyTokens(board.boxes[30]);
		};
		actionRoutines[31] = (string myPlayerName) {
			//Business card
			board.players[myPlayerName].takeBusinessCard();
		};
		actionRoutines[32] = (string myPlayerName) {
			//Dalmine
			board.players[myPlayerName].buyTokens(board.boxes[32]);
		};
		actionRoutines[33] = (string myPlayerName) {
			//Enel
			board.players[myPlayerName].buyTokens(board.boxes[33]);
		};
		actionRoutines[34] = (string myPlayerName) {
			//Olivetti
			board.players[myPlayerName].buyTokens(board.boxes[34]);
		};
		actionRoutines[35] = (string myPlayerName) {
			//Raw materials explosion
			board.players[myPlayerName].accident(board.boxes[35]);
		};
		actionRoutines[36] = (string myPlayerName) {
			//Dalmine
			board.players[myPlayerName].buyTokens(board.boxes[36]);
		};
		actionRoutines[37] = (string myPlayerName) {
			//Enel
			board.players[myPlayerName].buyTokens(board.boxes[37]);
		};
		actionRoutines[38] = (string myPlayerName) {
			//Olivetti
			board.players[myPlayerName].buyTokens(board.boxes[38]);
		};
		actionRoutines[39] = (string myPlayerName) {
			//Alitalia (sale products) 1
			board.players[myPlayerName].sellProducts(board.boxes[39]);
		};
		actionRoutines[40] = (string myPlayerName) {
			//Postal Market (sale products) 1
			board.players[myPlayerName].sellProducts(board.boxes[40]);
		};
		actionRoutines[41] = (string myPlayerName) {
			//Standa (sale products) 1
			board.players[myPlayerName].sellProducts(board.boxes[41]);
		};
		actionRoutines[42] = (string myPlayerName) {
			//Fire in products stock
			board.players[myPlayerName].accident(board.boxes[42]);
		};
		actionRoutines[43] = (string myPlayerName) {
			//Alitalia (sale products) 2
			board.players[myPlayerName].sellProducts(board.boxes[43]);
		};
		actionRoutines[44] = (string myPlayerName) {
			//Postal Market (sale products) 2
			board.players[myPlayerName].sellProducts(board.boxes[44]);
		};
		actionRoutines[45] = (string myPlayerName) {
			//Standa (sale products) 2
			board.players[myPlayerName].sellProducts(board.boxes[45]);
		};
		actionRoutines[46] = (string myPlayerName) {
			//Business card
			board.players[myPlayerName].takeBusinessCard();
		};
		actionRoutines[47] = (string myPlayerName) {
			//Alitalia (sale products) 3
			board.players[myPlayerName].sellProducts(board.boxes[47]);
		};
		actionRoutines[48] = (string myPlayerName) {
			//Postal Market (sale products) 3
			board.players[myPlayerName].sellProducts(board.boxes[48]);
		};
		actionRoutines[49] = (string myPlayerName) {
			//Standa (sale products) 3
			board.players[myPlayerName].sellProducts(board.boxes[49]);
		};
	}
	
	private void fillBoard (string[] columns, string[] data) 
	in {
		/*ATTENZIONE L'inserimento della casella nella board
		presume che la prima colonna dei dati della
		query sia l'id della casella*/
		assert(columns[0] == "id", assMissingBoardId);
	}
	body {
		uint boxPosition;
		foreach (r; 0 .. (data.length / columns.length)) {
			foreach (c, column; columns) {
				boxPosition = to!uint(data[columns.length * r]);
				boxes[boxPosition][column] = data[(columns.length * r) + c];
			}
		}
	}
	
	@property Board clone() {
		return this;
	}
}

//Viewer models
class PlayersList : ListStore
{
	private uint size;
	
	this(bool showIcons = false)
	{
		//Pixbuf.getType()
		if (showIcons)
			super([Pixbuf.getType(), GType.STRING, GType.STRING]);
		else
			super([GType.STRING, GType.STRING]);
		this.size = uint.init;
	}

	@property uint length() {
		return size;
	}
	
	public void addPlayer(in string name, in string address)
	{
		TreeIter iter = createIter();
		setValue(iter, 0, name);
		setValue(iter, 1, address);
		++size;
	}
	
	public void addPlayer(in string name, in string address, Image icon)
	{
		TreeIter iter = createIter();
		setValue(iter, 0, new Value(icon.getPixbuf()));
		setValue(iter, 1, name);
		setValue(iter, 2, address);
		++size;
	}
}
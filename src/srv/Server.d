import std.stdio;
import std.socket;
import std.conv;
import std.format;
import std.json;
import std.algorithm;
import std.regex;
import std.range;
import std.string;
import std.concurrency;
import std.random;
import std.exception;
import core.thread;
import Msg;
import Network;

private {
	struct Turn {
		Socket[] players;
		
		this (Socket[] players) {
			this.players = players;
		}
		
		@property Socket front() {
			return players[0];
		}
		
		@property Turn save() {
			return this;
		}
		
		@property bool empty() const {
			return players.length == 0;
		}
		
		void popFront() {
			players = players[1 .. $];
		}
		
		Socket opCall() {
			Socket first = this.front();
			popFront();
			return first;
		}
		
		void toString(void delegate(const (char)[]) d) {
			foreach (player; players)
				formattedWrite(d, "[%s]", player.remoteAddress());
		}
	}
	
	Socket pickOne(ref Turn nextPlayer, Socket[] sockets) {
		if (nextPlayer.empty)
			nextPlayer = Turn(sockets);
		return nextPlayer();
	}
	
	void buildSelectList(ref SocketSet list, Socket[] socks) {
		list.reset();
		foreach (sock; socks)
			list.add(sock);
	}

	void echo(ref Socket[] dest, in string data, in Socket noEcho = null, bool verbose = true) {
		foreach (sock; dest) {
			if ((noEcho is null)||(sock != noEcho)) {
				sock.sendAll(data);
				if (verbose)
					writefln("ECHO to %s: %s", sock.remoteAddress(), data);
			}
		}
	}
	
	void handleNewConnections(ref Socket listener, ref Socket[] sockets, ref SocketSet list) {
		if (list.isSet(listener)) {
			Socket newClient = listener.accept();
			sockets ~= newClient;
			list.add(newClient);
			writefln("Client %s connesso", newClient.remoteAddress());
		}
	}
	
	void disconnect(ref Socket[] sockets, ref SocketSet list, ref string[Socket] clientNames, uint pos) {
		auto socket = sockets[pos];
		socket.shutdown(SocketShutdown.BOTH);
		list.remove(socket);
		clientNames.remove(socket);
		sockets = remove(sockets, pos);
		writefln("Client %s disconnesso", socket.remoteAddress());
		socket.close();
	}
	
	void writeSockets(Socket[] sockets) {
		each!(
			(i) {
				writef("[%s]", i.remoteAddress);
			    }
		)(sockets);
		/*for (int i = 0 ; i < sockets.length ; ++i)
			writef("[%s]", sockets[i].remoteAddress);*/
		writeln();
	}
	
	void gameOver(Socket winner, ref Socket[] sockets, Socket[] spectators, string[Socket] clientNames) {
		echo(spectators, srvCmdMsgAdmin ~ format("Il giocatore %s ha vinto", clientNames[winner]));
		echo(sockets, srvCmdMsgAdmin ~ format("Il giocatore %s ha vinto", clientNames[winner]));
		writefln("Il giocatore %s ha vinto", clientNames[winner]);
	}
	
	bool isGameOver(ref Socket[] sockets, Socket[] spectators, string[Socket] clientNames) {
		bool gameEnded = false;
		//TODO
		// Put customizable condition for game over
		if (clientNames.length < 2) {
			Socket winner = sockets[0];
			echo(spectators, srvCmdMsgAdmin ~ format("Il giocatore %s ha vinto", clientNames[winner]));
			echo(sockets, srvCmdMsgAdmin ~ format("Il giocatore %s ha vinto", clientNames[winner]));
			writefln("Il giocatore %s ha vinto", clientNames[winner]);
			gameEnded = true;
		}
		return gameEnded;
	}
	
	void buildSequence(ref uint[] sequence, uint len) {
		foreach(i; 0 .. len)
			sequence ~= i;
		Mt19937 gen;
		gen.seed(unpredictableSeed);
		randomShuffle(sequence, gen);
	}
	
	//Returns zero if the dice is "OPA SCAD" otherwise it will return the percentage number (negative or positive)
	int getOpaDiceValue (string opaValue) {
		int result;
		try {
			result = to!int(chop(opaValue));
		}
		catch (Exception e)
			result = 0;
		return result;
	}
	
	//Returns "EXPIRED" string if at least one of the dice is OPA SCAD otherwise it will return a string containing the arithmetic sum of dices percentage
	string getOpaResult (string whiteDice, string blackDice) {
		int whiteValue = getOpaDiceValue(whiteDice);
		int blackValue = getOpaDiceValue(blackDice);
		string result;
		if ((whiteValue == 0)||(blackValue == 0))
			result = "EXPIRED";
		else
			result = to!string(whiteValue + blackValue);
		return result;
	}
}

int main(string[] argv)
{
	ushort serverPort = to!ushort(argv[1]);
	bool gameStarted = false;
	enforce((argv.length >= 2), excNotEnoughServerArguments);
	
	Socket[] sockets;
	auto allSockets = new SocketSet(7);
	allSockets.reset();
	Socket listener = new Socket(AddressFamily.INET, SocketType.STREAM, ProtocolType.TCP);
	listener.setOption(SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, true);
	listener.bind(new InternetAddress(serverPort));
	listener.listen(6);
	
	string[Socket] clientNames;
	Socket[string] clientSockets;
	
	//OPA players
	string thrower, owner;
	
	//OPA dice values
	string blackDice, whiteDice;
	
	Turn nextPlayer;
	int maxDicesSoFar;
	Socket firstPlayerSoFar;
	uint firstPlayerIndex;
 	Socket[] spectators;
 	uint[] conjunctureSequence, businessSequence;
  	buildSequence(conjunctureSequence, numConjunctureCards);
  	buildSequence(businessSequence, numBusinessCards);
  	bool autoRefresh = false;

	while(true) {
		//Resetta e ricostruisce la lista select dei socket
		buildSelectList(allSockets, sockets);
		allSockets.add(listener);
		//Select	
		try {
			int res = Socket.select(allSockets, null, null);
		}
		catch (SocketException s) {
			writeln("Errore nella select dei socket.\n%s", s);
			return 1;
		}
		if (!gameStarted) {
			handleNewConnections(listener, sockets, allSockets);
			nextPlayer = Turn(sockets);
		}
		//Sends players list to all sockets for waitingRoom window autorefresh list
		/*if (autoRefresh) {
			echoPlayersList(sockets, clientNames);
			writeln("Sent playersList");
		}*/
		//Ready sockets loop
		foreach (pos, socket; sockets) {
			if (!allSockets.isSet(socket))
				continue;
				
			char[][] packets;
			writefln("In attesa di dati dal client %s ...", socket.remoteAddress());
			auto received = socket.receiveAll(packets);
			//assert((!nextPlayer.empty), assTurnEmpty);
			
			switch (received) {
				case 0, Socket.ERROR:
					disconnect(sockets, allSockets, clientNames, to!uint(pos));
					//Rimuovo il socket anche dalla lista spettatori se presente
					spectators = remove(spectators, countUntil(spectators, socket));
					nextPlayer = Turn(sockets);
					if (gameStarted)
						isGameOver(sockets, spectators, clientNames);
				break;
				default:
					//Interpreting received packets
					foreach (packet; packets) {
						//Initialize intepreter
						mixin Interpreter!server;
						writefln("Ricevuto il comando %s dal client %s\n", packet, socket.remoteAddress());
						evaluate(matchCommand(packet, syntax));
					}
				break;
			}
		}
	}
}


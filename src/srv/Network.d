module Network;
import std.socket;
import std.algorithm;
import std.exception;
import std.json;
import std.conv;
import std.stdio;
import std.format;
import std.array;
import std.string;
import std.regex;
import core.thread;
import jsonizer;

enum {
	numConjunctureCards = 33,
	numBusinessCards = 33,
	excNotEnoughArguments = "Parametri mancanti: <indirizzo> <porta>",
	excNotEnoughServerArguments = "Parametri mancanti: <porta>",
	excNameNotSent = "Impossibile inviare il nome al server",
	excStartNotSent = "Impossibile avviare il gioco, il server non risponde",
	glbMsgLostPlayer = "Il giocatore %s è fallito",
	assTurnEmpty = "La lista dei turni giocatore è vuota",
	server = "server",
	nTalker = "nTalker",
	controller = "controller",

	//Protocollo di comunicazione, comandi e risposte
	plyCmdName = "NAME: ",
	plyCmdStart = "START",
	plyCmdDices = "DICES: ",
	plyCmdEndTurn = "ENDTURN: ",
	plyCmdHigherDices = "HIGHERDICES",
	plyCmdLost = "LOST",
	plyCmdPlayerUpdate = "PLAYERUPDATE: ",
	plyCmdConjuncture = "CONJUNCTURE",
	plyCmdBusiness = "BUSINESS",
	plyCmdGetPlayersList = "GETPLAYERSLIST",
	plyCmdMsg = "MSG: ",
	plyCmdDInfo = "DINFO: ",
	plyCmdNotify = "NOTIFY: ",
	plyCmdThrowOpa = "THROWOPA: ",
	plyCmdOpaWhite = "OPAWHITE: ",
	plyCmdOpaBlack = "OPABLACK: ",
	plyCmdOpaTerminated = "OPATERMINATED",
	srvRspTurn = "TURN",
	srvRspBegin = "BEGIN: ",
	srvRspNameOk = "NAMEOK",
	srvRspNotEnoughPlayers = "NOT ENOUGH PLAYERS",
	srvRspUpdateInfo = "UPDATEINFO: ",
	srvRspPlayersList = "PLAYERSLIST: ",
	srvRspAlert = "ALERT: ",
	srvRspThrowerDice = "THROWERDICE: ",
	srvRspOwnerDice = "OWNERDICE: ",
	srvCmdMsgAdmin = "MSGADMIN: ",
	srvCmdPlay = "PLAY",
	srvCmdCSequence = "CSEQUENCE: ",
	srvCmdBSequence = "BSEQUENCE: ",
	srvCmdOpa = "OPA: ",
	srvCmdSpectatorOpa = "SPECTATOROPA: ",
	srvCmdOpaResult = "OPARESULT: "
}


/*
	Network protocol syntax
	Key: id command string
	Value: regular expression for protocol syntax
*/

enum string[string] syntax = [
	plyCmdName : r"^NAME: ",
	plyCmdStart : r"^START$",
	plyCmdDices : r"^DICES: ",
	plyCmdDInfo : r"^DINFO: ",
	plyCmdHigherDices : r"^HIGHERDICES$",
	plyCmdMsg : r"^MSG: ",
	plyCmdEndTurn : r"^ENDTURN: ",
	plyCmdLost : r"^LOST$",
	plyCmdPlayerUpdate : r"^PLAYERUPDATE: ",
	plyCmdConjuncture : r"^CONJUNCTURE$",
	plyCmdBusiness : r"^BUSINESS$",
	plyCmdGetPlayersList : r"^GETPLAYERSLIST$",
	plyCmdNotify : r"^NOTIFY: ",
	plyCmdThrowOpa : r"^THROWOPA: ",
	plyCmdOpaWhite : r"^OPAWHITE: ",
	plyCmdOpaBlack : r"^OPABLACK: ",
	plyCmdOpaTerminated : r"^OPATERMINATED$",
	srvRspTurn : r"^TURN$",
	srvRspBegin : r"^BEGIN: ",
	srvRspNameOk : r"^NAMEOK$",
	srvRspNotEnoughPlayers : r"^NOT ENOUGH PLAYERS$",
	srvRspAlert : r"^ALERT: ",
	srvRspThrowerDice : r"^THROWERDICE: ",
	srvRspOwnerDice : r"^OWNERDICE: ",
	srvCmdMsgAdmin : r"^MSGADMIN: ",
	srvCmdPlay : r"^PLAY$",
	srvCmdOpa : r"^OPA: ",
	srvCmdCSequence : r"^CSEQUENCE: ",
	srvCmdBSequence : r"^BSEQUENCE: ",
	srvCmdSpectatorOpa : r"^SPECTATOROPA: ",
	srvCmdOpaResult : r"^OPARESULT: ",
	srvRspUpdateInfo : r"^UPDATEINFO: ",
	srvRspPlayersList : r"^PLAYERSLIST: "
];

//Network protocol interpreter
mixin template Interpreter(alias context)
{
	//Protocol syntax checking loop
	string matchCommand(const(char)[] packet, string[string] syntax) {
		foreach (cmd, regExp; syntax) {
			if (packet.matchFirst(regExp))
				return cmd;
		}
		return packet.idup;
	}
	
	static if (context == "server") {
		//Code to execute for given protocol command
		void evaluate(string commandId)
		{
			char[] data = packet;
			switch(commandId) {
				case plyCmdName :
					string name;
					formattedRead(data, plyCmdName ~ "%s", &name);
					clientNames[socket] = name;
					clientSockets[name] = socket;
					socket.sendAll(srvRspNameOk);
					//autoRefresh = true;
					writefln("Sent %s to %s", srvRspNameOk, socket.remoteAddress);
				break;
				case plyCmdStart :
					if (clientNames.length < 2) {
						socket.sendAll(srvRspNotEnoughPlayers);
						writefln("Sent %s to %s", srvRspNotEnoughPlayers, socket.remoteAddress);
					}
					else {
						string[string] clientInfo;
						//autoRefresh = false;
						//Building players names / players address info
						foreach(playerSocket, playerName; clientNames) {
							string addressPort = playerSocket.remoteAddress.toAddrString() ~ ":" ~
							playerSocket.remoteAddress.toPortString();
							clientInfo[addressPort] = playerName;
						}
						//Manda a tutti i client la lista dei giocatori
						echo(sockets, srvRspBegin ~ serialize(clientInfo));
						//echo(sockets, srvRspBegin ~ serialize(clientNames.values));
						gameStarted = true;
					}
				break;
				case plyCmdDices :
					string payload;
					formattedRead(data, plyCmdDices ~ "%s", &payload);
					int[] dices = deserialize!(int[])(payload);
					//Informa gli altri giocatori dei dadi lanciati
					echo(sockets, format(srvCmdMsgAdmin ~ msgPlayerDiceNum, clientNames[socket], dices));
					int number = sum(dices);
					if (number > maxDicesSoFar) {
						maxDicesSoFar = number;
						firstPlayerSoFar = socket;
						firstPlayerIndex = to!uint(pos);
					}
					if (nextPlayer.empty) {
						echo(sockets, format(srvCmdMsgAdmin ~ msgFirstPlayer, clientNames[firstPlayerSoFar]));
						//Mette alla prima posizione il giocatore che ha fatto il numero piu alto
						swap(sockets[0], sockets[firstPlayerIndex]);
						nextPlayer = Turn(sockets);
						//INIZIA IL GIOCO VERO E PROPRIO
						//Manda a tutti i client la sequenza random per il mazzo carte congiuntura e vita degli affari
						echo(sockets, srvCmdCSequence ~ serialize(conjunctureSequence), null, false);
						conjunctureSequence.popFront();
						echo(sockets, srvCmdBSequence ~ serialize(businessSequence), null, false);
						Socket player = nextPlayer.pickOne(sockets);
						//Sending next turn message to all players
						//echo(sockets, format(srvCmdMsgAdmin ~ msgTurn, clientNames[player]));
						player.sendAll(srvCmdPlay);
						writefln("Sent %s to %s", srvCmdPlay, player.remoteAddress);
					}
					else {
						Socket player = nextPlayer();
						//Informa tutti i giocatori che è il turno di clientNames[player]
						echo(sockets, format(srvCmdMsgAdmin ~ msgTurn, clientNames[player]));
						player.sendAll(srvRspTurn);
						writefln("Sent %s to %s", srvRspTurn, player.remoteAddress);
					}
				break;
				case plyCmdHigherDices :
					if (!nextPlayer.empty) {
						nextPlayer = Turn(sockets);
						Socket player = nextPlayer();
						//Informa tutti i giocatori che è il turno di clientNames[player]
						echo(sockets, format(srvCmdMsgAdmin ~ msgTurn, clientNames[player]));
						player.sendAll(srvRspTurn);
						writefln("Sent %s to %s", srvRspTurn, player.remoteAddress);
					}
				break;
				case plyCmdDInfo, plyCmdMsg :
					echo(sockets, data.idup, socket);
				break;
				case plyCmdEndTurn :
					string payload;
					formattedRead(data, plyCmdEndTurn ~ "%s", &payload);
					//Invio gli aggiornamenti sul gioco sia ai giocatori che agli spettatori
					echo(sockets, srvRspUpdateInfo ~ payload, socket);
					//echo(spectators, srvRspUpdateInfo ~ payload, socket);
					Socket player = nextPlayer.pickOne(sockets);
					echo(sockets, format(srvCmdMsgAdmin ~ msgTurn, clientNames[player]));
					player.sendAll(srvCmdPlay);
					writefln("Sent %s to %s", srvCmdPlay, player.remoteAddress);
				break;
				case plyCmdLost :
					echo(sockets, srvCmdMsgAdmin ~ format(glbMsgLostPlayer, clientNames[socket]));
					clientNames.remove(socket);
					sockets = remove(sockets, countUntil(sockets, socket));
					spectators ~= socket;
					Socket player = nextPlayer.pickOne(sockets);
					if (!isGameOver(sockets, spectators, clientNames)) {
						echo(sockets, format(srvCmdMsgAdmin ~ msgTurn, clientNames[player]));
						player.sendAll(srvCmdPlay);
						writefln("Sent %s to %s", srvCmdPlay, player.remoteAddress);
					}
				break;
				case plyCmdPlayerUpdate :
					string payload;
					formattedRead(data, plyCmdPlayerUpdate ~ "%s", &payload);
					//Invio gli aggiornamenti sul gioco sia ai giocatori che agli spettatori
					echo(sockets, srvRspUpdateInfo ~ payload, socket);
					echo(spectators, srvRspUpdateInfo ~ payload, socket);
				break;
				case plyCmdConjuncture :
					//writeln("Sequenza: ", conjunctureSequence);
					if (conjunctureSequence.length == 0) {
						buildSequence(conjunctureSequence, numConjunctureCards);
						echo(sockets, srvCmdCSequence ~ serialize(conjunctureSequence), null, false);
					}
					else
						echo(sockets, plyCmdConjuncture, socket);
					conjunctureSequence.popFront();
				break;
				case plyCmdBusiness :
					/*
						NOTE: Business deck does not have an uncovered upCard so 
						for every new shuffle sequence all players don't take 
						a new card like in conjuncture.
					*/
					if (businessSequence.length == 0) {
						buildSequence(businessSequence, numBusinessCards);
						echo(sockets, srvCmdBSequence ~ serialize(businessSequence), null, false);
					}
					else {
						/*So now only when receiving a new business card command
						we popFront*/
						echo(sockets, plyCmdBusiness, socket);
						businessSequence.popFront();
					}
				break;
				case plyCmdGetPlayersList :
					string[string] clientInfo;
					//Building players names / players address info
					foreach(playerSocket, playerName; clientNames) {
						string addressPort = playerSocket.remoteAddress.toAddrString() ~ ":" ~
						playerSocket.remoteAddress.toPortString();
						clientInfo[addressPort] = playerName;
					}
					socket.sendAll(srvRspPlayersList ~ serialize(clientInfo));
					writeln("Sent ", srvRspPlayersList ~ serialize(clientInfo));
				break;
				case plyCmdNotify :
					string message;
					formattedRead(data, plyCmdNotify ~ "%s", &message);
					echo(sockets, srvRspAlert ~ message);
				break;
				case plyCmdThrowOpa :
					string serializedInfo;
					formattedRead(data, plyCmdThrowOpa ~ "%s", &serializedInfo);
					auto opaInfo = deserialize!(string[string])(serializedInfo);
					
					//We save thrower and owner player names
					thrower = opaInfo["throwerName"];
					owner = opaInfo["ownerName"];
					
					Socket ownerSocket = clientSockets[owner];
					//We send all opa information to the owner
					ownerSocket.sendAll(srvCmdOpa ~ serializedInfo);
					//Sends spectator opa command to other players (not thrower and owner)
					foreach (s; sockets) {
						if ((clientNames[s] != thrower)&&(clientNames[s] != owner))
							s.sendAll(srvCmdSpectatorOpa ~ serialize(opaInfo));
					}
						
				break;
				case plyCmdOpaWhite :
					//Here we calculate the OPA result, the owner has thrown the dice
					string opaDiceValue;
					formattedRead(data, plyCmdOpaWhite ~ "%s", &opaDiceValue);
					whiteDice = opaDiceValue;
					echo(sockets, srvRspOwnerDice ~ opaDiceValue, socket);
					//Then we send the opaResult calculated
					echo(sockets, srvCmdOpaResult ~ getOpaResult(whiteDice, blackDice));
				break;
				case plyCmdOpaBlack :
					string opaDiceValue;
					formattedRead(data, plyCmdOpaBlack ~ "%s", &opaDiceValue);
					blackDice = opaDiceValue;
					echo(sockets, srvRspThrowerDice ~ opaDiceValue, socket);
				break;
				case plyCmdOpaTerminated :
					echo(sockets, plyCmdOpaTerminated, socket);
				break;
				default:
					writefln("Server: Unable to evaluate <%s> command from %s client", commandId, socket);
				break;
			}
		}
	}
	else static if (context == "nTalker") {
		//Code to execute for given protocol command
		void evaluate(string commandId)
		{
			char[] data = packet;
			Tid eListenerThread = locate("eListenerThread");
			Tid gui = locate("guiThread");
			switch(commandId) {
				case srvRspUpdateInfo, 
				     srvCmdCSequence, srvCmdBSequence, srvRspNameOk,
				     plyCmdConjuncture, plyCmdBusiness, plyCmdDInfo, 
				     srvCmdOpa, srvCmdSpectatorOpa, srvCmdOpaResult,
				     plyCmdOpaTerminated:
					ownerTid.send(packet.idup);
				break;
				//Received a Chat Message
				case plyCmdMsg :
					string message;
					formattedRead(data, plyCmdMsg ~ "%s", &message);
					//Debug write
					writeln("Chat message: ", message);
					gui.sendRequest(UI.renderMessage, message);
				break;
				case srvCmdMsgAdmin :
					string msg;
					formattedRead(packet, srvCmdMsgAdmin ~ "%s", &msg);
					gui.sendRequest(UI.printMessage, msg);
				break;
				case srvRspAlert :
					string msg;
					formattedRead(packet, srvRspAlert ~ "%s", &msg);
					gui.sendRequest(UI.alert, msg);
				break;
				case srvRspBegin:
					//Director has already closed the window starting the game so all the begin packet will be passed to main thread (Controller.d)
					if (director)
						ownerTid.send(packet.idup);
					else {
						//Signals the guest user that the game has started, so the waiting room will be closed (Viewer.d)
						gui.send(packet.idup);
					}
				break;
				case srvRspNotEnoughPlayers :
					version(gtk){
						/*if (director) {
							Tid starter = locate("starter");
							starter.send(packet.idup);
						}*/
					}
					/*else
						sendStartGame(server, iAmDirector);*/
				break;
				case srvRspTurn :
					version(gtk) {
						//signal eListener to wait for user input
						eListenerThread.send(srvRspTurn);
						gui.sendRequest(UI.enableDiceButton, null);
					}
					/+else {
						//No need to wait for event signal for textonly version
						//Notify mainThread for new turn
						ownerTid.send(srvRspTurn, "dice");
					}+/
				break;
				case srvCmdPlay :
					version(gtk) {
						//signal eListener to wait for user input
						eListenerThread.send(srvCmdPlay);
						gui.sendRequest(UI.enableUserButtons, null);
					}
					/+else {
						//Notify to mainThread for a new play turn
						ownerTid.send(srvCmdPlay, "dice");
					}+/
				break;
				case srvRspPlayersList : 
					string list;
					formattedRead(packet, srvRspPlayersList ~ "%s", &list);
					//Send to guiThread (in Viewer.d) the new list
					gui.send(list);
				break;
				case srvRspThrowerDice :
					string opaValue;
					formattedRead(packet, srvRspThrowerDice ~ "%s", &opaValue);
					string[] params = [opaValue, to!string(to!int(opaDice.black))];
					gui.sendRequest(opaUI.renderOpaDice, serialize(params));
					//gui.sendRenderOpaDiceRequest(opaValue, opaDice.black);
					/*
					The owner receives the thrower dice, now its time to enable the
					opa dice button
					*/
					gui.sendRequest(opaUI.enableOpaDice, null);
				break;
				case srvRspOwnerDice:
					string opaValue;
					formattedRead(packet, srvRspOwnerDice ~ "%s", &opaValue);
					string[] params = [opaValue, to!string(to!int(opaDice.white))];
					gui.sendRequest(opaUI.renderOpaDice, serialize(params));
					//gui.sendRenderOpaDiceRequest(opaValue, opaDice.white);
					// Here we can't enable the dice button, first we need to get the command from the server to do another OPA match (if we want)
				break;
				default:
					writefln("nTalker: Unable to evaluate <%s> command from the server", commandId);
				break;
			}
		}
	}
	else static if (context == "controller") {
		//Code to execute for given protocol command
		void evaluate(string commandId)
		{
			switch(commandId) {
				case srvRspBegin :
					string payload;
					formattedRead(receivedCmd, srvRspBegin ~ "%s", &payload);
					auto clients = deserialize!(string[string])(payload);
					writeln(clients);
					game.start(clients, name);
					myPlayer = game.board.players[name];
					version(gtk) {
						immutable(Board) roBoard = cast(immutable)game.board;
						gui.send(roBoard);
						writeln("sent to gui the board!");
					}
					sendStartHigherNumberDices(server, iAmDirector);
				break;
				case srvRspUpdateInfo :
					string payload;
					formattedRead(receivedCmd, srvRspUpdateInfo ~ "%s", &payload);
					Player updatedPlayer = deserialize!Player(payload);
					game.board.players[updatedPlayer.name].updateWith(updatedPlayer);
					version (gtk) {
						//Refresh new game status to output graphics
						gui.sendRequest(UI.fullRefreshPlayer, updatedPlayer.name);
					}
					/*else
						screen.renderAllPlayers();*/
				break;
				case srvCmdCSequence :
					string payload;
					formattedRead(receivedCmd, srvCmdCSequence ~ "%s", &payload);
					auto newSequence = deserialize!(uint[])(payload);
					game.board.conjuncture.shuffle(newSequence);
					//After new card shuffle i MUST take a new card
					game.board.conjuncture.takeOneCard();
					gui.sendRequest(UI.printConjuncture, game.board.conjuncture.upCard.text);
				break;
				case srvCmdBSequence :
					string payload;
					formattedRead(receivedCmd, srvCmdBSequence ~ "%s", &payload);
					auto newSequence = deserialize!(uint[])(payload);
					game.board.business.shuffle(newSequence);
					//No need to take new card after shuffle like in conjuncture
				break;
				case plyCmdConjuncture :
					//Current turn player has taken one card, we must take the card too
					game.board.conjuncture.takeOneCard();
					gui.sendRequest(UI.printConjuncture, game.board.conjuncture.upCard.text);
				break;
				case plyCmdBusiness :
					businessCard card = game.board.business.takeOneCard();
					gui.sendRequest(UI.alert, card.text);
				break;
				case plyCmdDInfo :
					//Rendering dices thrown by the turning player
					string payload;
					formattedRead(receivedCmd, plyCmdDInfo ~ "%s", &payload);
					int[] dices = deserialize!(int[])(payload);
					gui.sendRequest(UI.renderDices, serialize(dices));
				break;
				case srvCmdOpa :
					//We set opa status as running
					game.board.opaRunning = true;
					//We setup all new opa information to our Board
					string serializedInfo;
					formattedRead(receivedCmd, srvCmdOpa ~ "%s", &serializedInfo);
					game.board.opaInfo = deserialize!(string[string])(serializedInfo);
					View.opaDialog(opaDice.white);
				break;
				case srvCmdSpectatorOpa :
					//We setup all new opa information to our Board
					string serializedInfo;
					formattedRead(receivedCmd, srvCmdSpectatorOpa ~ "%s", &serializedInfo);
					game.board.opaInfo = deserialize!(string[string])(serializedInfo);
					View.opaDialog(opaDice.spectator);
				break;
				case srvCmdOpaResult :
					string throwerName = game.board.opaInfo["throwerName"];
					string ownerName = game.board.opaInfo["ownerName"];
					string opaResult;
					
					Player owner = game.board.players[ownerName];
					Player thrower = game.board.players[throwerName];
					
					uint shareId = to!uint(game.board.opaInfo["shareId"]);
					immutable Share opaShare = game.board.shares[shareId];
					
					formattedRead(receivedCmd, srvCmdOpaResult ~ "%s", &opaResult);
					writefln("OPA RESULT FOR %s: %s", opaShare.name, opaResult);
					writeln("THROWER: ", throwerName);
					writeln("OWNER: ", ownerName);
					
					if (opaResult == "EXPIRED") {
						//The thrower has lost the match
						//pay to owner the tax
						thrower.pay(opaShare.expiredOpa, owner);
						//Refresh thrower cash amount to OPA window
						gui.sendRequest(opaUI.refreshThrowerCash, to!string(thrower.cash));
						//Show a message with opa result
						gui.sendRequest(opaUI.showOpaResult, format(msgOpaExpired, throwerName, opaShare.expiredOpa, ownerName));
						//NOTE: ONLY if i am the thrower then enable again opa dice button and close button
						if (myPlayer.name == throwerName) {
							gui.sendRequest(opaUI.enableOpaDice, null);
							gui.sendRequest(opaUI.enableCloseButton, null);
						}
					}
					else {
						//The thrower has won the match
						//buy the share with the opa result percentage from the owner
						uint percentage = 100 + to!int(opaResult);
						thrower.purchase(opaShare, percentage, true);
						//Refresh thrower cash amount to OPA window
						gui.sendRequest(opaUI.refreshThrowerCash, to!string(thrower.cash));
						//Show a message with opa result
						gui.sendRequest(opaUI.showOpaResult, format(msgOpaCompleted, throwerName, opaShare.name, percentage));
						//We signal to all players that the Opa has finished
						server.sendAll(plyCmdOpaTerminated);
					}
				break;
				case plyCmdOpaTerminated:
					game.board.opaRunning = false;
					gui.sendRequest(opaUI.enableCloseButton, null);
				break;
				case srvRspTurn :
					int[] dices;
					version(gtk) {
						switch(signal) { 
							case "dice" : //button dices clicked
								dices = myPlayer.throwDices(game, false);
								string srzDices = serialize(dices);
								//Sending just thrown dices to the server
								server.sendAll(plyCmdDInfo ~ srzDices);
								//Refresh dices value to output
								gui.sendRequest(UI.renderDices, srzDices);
								server.sendAll(plyCmdDices ~ srzDices);
							break;
							default :
								writeln("<mainThread[Turn]> received unknown signal: ", signal);
						}
					}
					/*else {
						dices = myPlayer.throwDices(game, false);
						screen.renderDices(dices);
					}*/
				break;
				case srvCmdPlay :
					int[] dices;
					version(gtk) {
						switch(signal) {
							case "dice" : //button dices clicked
								dices = myPlayer.throwDices(game, true);
								//Refresh dices value to output
								gui.sendRequest(UI.renderDices, serialize(dices));
								//Refresh player
								gui.sendRequest(UI.fullRefreshPlayer, myPlayer.name);
								game.boxAction(myPlayer.name);
								//Sending just thrown dices to the server
								server.sendAll(plyCmdDInfo ~ serialize(dices));
								if ((!myPlayer.lost)&&(!game.board.opaRunning))
									server.sendAll(plyCmdEndTurn ~ serialize(myPlayer));
							break;
							case "jwt" : 
								if (myPlayer.pos.useServiceToken(serviceToken.jwt, myPlayer.jwt, myPlayer.sintex)) {
									//Refresh new game status to output graphics
									gui.sendRequest(UI.fullRefreshPlayer, myPlayer.name);
									//Popup
									gui.sendRequest(UI.showPopupMessage, msgTokenUsed);
									//Execute box operation (it will be always a conjuncture box when using a service token)
									game.boxAction(myPlayer.name);
									//Let us send to the server and the other players our changes without ending the turn
									server.sendAll(plyCmdPlayerUpdate ~ serialize(myPlayer));
								}
								else
									gui.sendRequest(UI.showPopupMessage, format(msgNotEnoughTokens, "jwt"));
							break;
							case "sintex" : 
								if (myPlayer.pos.useServiceToken(serviceToken.sintex, myPlayer.jwt, myPlayer.sintex)) {
									//Refresh new game status to output graphics
									gui.sendRequest(UI.fullRefreshPlayer, myPlayer.name);
									//Popup
									gui.sendRequest(UI.showPopupMessage, msgTokenUsed);
									//Execute box operation (it will be always a conjuncture box when using a service token)
									game.boxAction(myPlayer.name);
									//Let us send to the server and the other players our changes without ending the turn
									server.sendAll(plyCmdPlayerUpdate ~ serialize(myPlayer));
								}
								else
									gui.sendRequest(UI.showPopupMessage, format(msgNotEnoughTokens, "sintex"));
							break;
							case "diceNum 1" :
								myPlayer.diceNum = 1;
							break;
							case "diceNum 2" :
								myPlayer.diceNum = 2;
							break;
							default :
								writeln("<mainThread[Play]> received unknown signal: ", signal);
						}
						
					}
					/*else {
						dices = myPlayer.throwDices(game, true);
						screen.renderDices(dices);
						game.boxAction(myPlayer.name);
						screen.renderAllPlayers();
						if (!myPlayer.lost)
							server.sendAll(plyCmdEndTurn ~ serialize(myPlayer));
					}*/
				break;
				default:
					writefln("Main: Unable to evaluate <%s> command from the server", receivedCmd);
				break;
			}
		}
	}
	else {
		void evaluate(string commandId) {
			writeln("Evaluation context \"%s\" does not exist!", context);
		}
	}
}

void debugNetworkShell(ref Socket server) {
	writeln("[DEBUG NETWORK SHELL]\n Debug shell to test Manager networking protocol.\n\n Type a protocol command for the server.\nPlease check the server output to be sure that messages are sent\n\n \"quit\" command will exit and continue the program execution");
	outer:
		while (true) {
			write("Debug shell > ");
			string cmd = chomp(readln());
			switch(cmd) {
				case "quit":
					break outer;
				default:
					char[][] packets;
					writeln(server.sendAll(cmd), " bytes sent to server");
					writeln("Waiting for server response ...");
					server.receiveAll(packets);
					writeln("-- PACKETS RECEIVED --");
					foreach (i, packet; packets)
						writefln("%s: %s", i, packet);
			}
		}
}

string serialize(T)(T data) {
	return to!string(toJSON(data));
}

T deserialize(T)(string data) {
	return (parseJSON(data)).fromJSON!(T);
}

/*Ripete send fino a quando questa non ha inviato tutto il buffer data e restituisce in byte i dati inviati (inclusi header)*/
uint sendAll(ref Socket sock, const (char)[] data) {
	uint nBytes = 0;
	string header = to!string(data.length);
	string total = header ~ "|" ~ data.idup;
	do {
		nBytes += to!uint(sock.send(total[nBytes .. $]));
	}
	while (nBytes < total.length);
	//Restituisce i bytes inviati
	return nBytes;
}
/*Restituisce su packets i pacchetti ricevuti (escludendo gli header) dai vari sendAll e restituisce i byte totali ricevuti (inclusi header) */
uint receiveAll(ref Socket sock, ref char[][] packets) {
	char[1500] allDataReceived;
	uint received = to!uint(sock.receive(allDataReceived));
	char[] temp = allDataReceived[0 .. received];
	char[] header, payload;
	uint totalReceived = received;
	uint packetLengthExpected, payloadLength, expectedPayloadLength;
	if (received > 0) {
		//Remember that we can read more than one send message per receive
		for (uint i = 0 ; i < totalReceived ; i += packetLengthExpected) {
			temp = allDataReceived[i .. totalReceived];
			formattedRead(temp, "%s|%s", &header, &payload);
			packetLengthExpected = to!uint(header) + to!uint(header.length) + 1;
			payloadLength = to!uint(payload.length);
			expectedPayloadLength = to!uint(header);
			//More receive, the first didnt load enough data
			while (payloadLength < expectedPayloadLength) {
				totalReceived += sock.receive(allDataReceived[totalReceived .. $]);
				formattedRead(temp, "%s|%s", &header, &payload);
				packetLengthExpected = to!uint(header) + to!uint(header.length) + 1;
				payloadLength = to!uint(payload.length);
				expectedPayloadLength = to!uint(header);
			}
			packets ~= payload[0 .. expectedPayloadLength].dup;
		}
	}
	return totalReceived;
}
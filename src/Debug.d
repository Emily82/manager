module Debug;

string debugInfo(string info = "") {
	return `
		debug {
			writeln("————————————————————————————————————");
			writeln("<", __FILE__ , "> ", __FUNCTION__ , ": ", "` ~ info ~ `");
		}
	       `;
}

CREATE TABLE "langs"(
id integer primary key autoincrement,
code varchar(5) not null,
unique(code));
;
CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE "banks"(
id integer primary key autoincrement,
name varchar(20) not null);
CREATE TABLE shares(
id integer primary key autoincrement,
name varchar(20) not null,
price integer not null,
unique(name));
;
CREATE TABLE servicetypes(
id integer primary key autoincrement,
share_id integer not null,
unique(share_id),
foreign key(share_id) references shares(id));
;
CREATE TABLE saletypes(
id integer primary key autoincrement,
share_id integer not null, numsalables integer not null default 1,
unique(share_id),
foreign key(share_id) references shares(id));
;
CREATE TABLE bankboxes(
id integer primary key autoincrement,
percentage integer not null,
bank_id integer not null,
foreign key(bank_id) references banks(id));
CREATE TABLE serviceboxes (
id integer primary key autoincrement,
price integer not null,
dividend integer not null,
servicetype_id integer not null,
foreign key(servicetype_id) references servicetypes(id));
CREATE TABLE productionboxes(
id integer primary key autoincrement,
price integer not null,
dividend integer not null,
productiontype_id integer not null,
foreign key(productiontype_id) references ProductionTypes(id));
CREATE TABLE shareboxes(
id integer primary key autoincrement,
share_id integer not null,
unique(share_id),
foreign key(share_id) references shares(id));
;
CREATE TABLE saleboxes(
id integer primary key autoincrement,
price integer not null,
dividend integer not null,
saletype_id integer not null,
foreign key(saletype_id) references saletypes(id));
CREATE TABLE companies (
id integer primary key autoincrement,
name varchar(20) not null);
CREATE TABLE board(
id integer primary key autoincrement,
bankbox_id integer default null,
servicebox_id integer default null,
productionbox_id integer default null,
sharebox_id integer default null,
salebox_id integer default null,
specialbox_id integer default null,
foreign key(bankbox_id) references bankboxes(id),
foreign key(servicebox_id) references serviceboxes(id),
foreign key(productionbox_id) references productionboxes(id),
foreign key(sharebox_id) references shareboxes(id),
foreign key(salebox_id) references saleboxes(id),
foreign key(specialbox_id) references specialboxes(id));
CREATE TABLE specialboxes(
id integer primary key autoincrement,
specialtype_id integer not null,
foreign key(specialtype_id) references SpecialTypes(id));
CREATE TABLE productiontypes (
    "id" INTEGER,
    "share_id" INTEGER NOT NULL
, "name" TEXT);
CREATE TABLE businessdeck(
id integer primary key autoincrement,
content_id integer not null,
foreign key(content_id) references contents(id));
CREATE TABLE conjuncturedeck(
id integer primary key autoincrement,
content_id integer not null,
foreign key(content_id) references contents(id));
CREATE TABLE specialtypes(
id integer primary key autoincrement,
content_id integer not null,
foreign key(content_id) references contents(id));
CREATE UNIQUE INDEX unique_numsalables on saletypes(numsalables);
CREATE TABLE contents (
id integer primary key autoincrement,
text text not null,
lang_id integer not null,
foreign key(lang_id) references langs(id));

# Manager
Manager, a board game based on financial and business world

## Dependencies
* libsqlite3-dev
* dub
* libgtkd3-dev

## Screenshots
![Alt text](screenshots/startGame.png?raw=true "Start Game")
![Alt text](screenshots/board.png?raw=true "Game board")
![Alt text](screenshots/opa.png?raw=true "OPA")

## Follow instructions to install dependencies and compile the game

Copy the following code to add the D language official apt-repository and to install Dub package manager and Dmd compiler
``` bash
sudo wget http://netcologne.dl.sourceforge.net/project/d-apt/files/d-apt.list -O /etc/apt/sources.list.d/d-apt.list
sudo apt-get update
sudo apt-get -y --allow-unauthenticated install --reinstall d-apt-keyring
sudo apt-get update
sudo apt-get install libsqlite3-dev dub dmd-bin libgtkd3-dev
```

###### Compile and Install
``` bash
make
sudo make install
```

## Server example
``` bash
managerServer 8000
```

## Client example
``` bash
Manager 127.0.0.1 8000
```